#!/usr/bin/env python3
import datetime
import glob
import os
import platform
import json
import random
import subprocess
import sys
from utils.command import run_batch_command, rmtree
from utils.sys_config import SysConfig
from export import Export
from utils.check_gpu_hang import check_gpu_hang
from options import Options
from repo_set import RevisionSpecification
from project_map import ProjectMap
from docker_runner import DockerRunner

drivers = ['iris', 'vk']


class PerfTester(object):
    def __init__(self, benchmark, iterations=2, discard=0,
                 env=None, custom_iterations_fn=None, windowed=False):
        self._benchmark = benchmark
        self._iterations = iterations
        self._discard = discard
        self._pm = ProjectMap()
        self._env = env
        self._opt = Options()
        if self._env is None:
            self._env = {}
        self._build_root = self._pm.build_root()
        self.driver = self._opt.hardware.split('_')[-1]
        if self.driver not in drivers:
            raise Exception("ERROR: unrecognized driver: " + self.driver)

        self.gpu_hw = self._opt.hardware.replace('_' + self.driver, '')
        prefix = self._build_root + "/lib/"
        self._env["LD_LIBRARY_PATH"] = prefix + ":" + prefix + "dri"
        self._env["LIBGL_DRIVERS_PATH"] = prefix + "dri"
        self._opt.update_env(self._env)
        self._custom_iterations_fn = custom_iterations_fn
        self._windowed = windowed
        self._sys_config = SysConfig()
        self._revspec = RevisionSpecification()
        self._mesa_ver = str(self._revspec.revision("mesa"))
        self._branchpoint = self._revspec.get_branchpoint("mesa")

    def build(self):
        # todo(majanes) possibly verify that benchmarks are in /opt
        pass

    def set_resolution(self):
        env = self._env
        # set the resolution to 1080p
        (out, _) = run_batch_command(["xrandr"],
                                     streamedOutput=False,
                                     quiet=True,
                                     env=env)
        for line in out.splitlines():
            words = line.decode().split()
            if words[1] != "connected" or words[2] == "secondary":
                continue
            run_batch_command(["xrandr", "--output", words[0], "--mode",
                               "1920x1080"], quiet=True, streamedOutput=False,
                              env=env)
        (out, _) = run_batch_command(["xdpyinfo"],
                                     quiet=True,
                                     streamedOutput=False,
                                     env=env)
        for line in out.splitlines():
            if "dimensions:" not in line.decode():
                continue
            if "1920x1080" not in line.decode():
                print("ERROR: could not set screen resolution to 1080p")
                print(line)
                assert(False)
            # else resolution is correct
            break

    def test(self):
        iterations = self._iterations
        save_dir = os.getcwd()
        if self._opt.hardware == "builder":
            print("ERROR: hardware must be set to a specific sku.  'builder' "
                  "is not a valid hardware setting.")
            assert(False)
        os.chdir(self._pm.project_source_dir("sixonix"))

        if not self._windowed:
            self.set_resolution()

        benchmarks = self._benchmark
        if type(benchmarks) != type([]):
            benchmarks = [self._benchmark]
        scores = dict([[b,[]] for b in benchmarks])

        # build a list of each benchmark to run
        bench_runs = []
        iterations = self._iterations
        it_multiplier = 1
        if self._opt.type == "daily":
            it_multiplier = 5
        for b in benchmarks:
            if self._custom_iterations_fn:
                iterations = self._custom_iterations_fn(b, self.gpu_hw) or iterations
            bench_runs += [b] * iterations * it_multiplier

        random.shuffle(bench_runs)
        iteration = 0
        self.set_freqs()
        for b in bench_runs:
            cmd = []
            interpreter = sys.executable
            cmd = [interpreter, "sixonix.py", "run"]
            cmd += [b]
            if not self._windowed:
                cmd += ["--fullscreen"]
            print(" ".join(cmd))
            try:
                (out, err) = run_batch_command(cmd, streamedOutput=False,
                                               env=self._env)
            except subprocess.CalledProcessError:
                # Handle crashed benchmark checking for gpu hang and quitting
                print("ERROR: Benchmark failed!")
                check_gpu_hang(False)
                sys.exit(-1)

            if err:
                print("err: " + err.decode())
            if iteration >= self._discard:
                if not out:
                    print("ERROR: Benchmark returned no score!")
                    check_gpu_hang(False)
                    sys.exit(-1)
                try:
                    scores[b].append(float(out.splitlines()[-1]))
                except ValueError:
                    print("ERROR: Benchmark returned no score!")
                    check_gpu_hang(False)
                    sys.exit(-1)
            iteration += 1
        os.chdir(save_dir)
        gpu_hw = self._opt.hardware.split("-")[0]
        gen, driver = gpu_hw.split('_')
        for b in benchmarks:
            benchmark = b
            # dots are changed to underscores since some downstream components
            # (e.g. javascript) cannot handle it.
            full_bench_name = benchmark.replace('.', '_')
            if self._windowed:
                full_bench_name += "_windowed"
            # gfxbench benchmarks that can do both gl and vk should have a
            # generic name since both drivers are represented by different
            # hardware types
            if 'gfxbench' in full_bench_name:
                full_bench_name = full_bench_name.replace('_gl_', '_').replace('_vk_', '_')
            result = {
                "benchmark": full_bench_name,
                "project": self._pm.current_project(),
                "gen": gen,
                "driver": driver,
                "mesa_ver": self._mesa_ver,
                "hostname": platform.node(),
                "scores": scores[benchmark],
                'sys_config': self._sys_config.get(),
                'sys_config_hash': self._sys_config.hash,
                'branchpoint': self._branchpoint,
            }
            out_dir = "/tmp/build_root/" + self._opt.arch + "/scores/" + \
                      full_bench_name + "/" + gpu_hw
            if not os.path.exists(out_dir):
                os.makedirs(out_dir)
            outf = out_dir + "/" + datetime.datetime.now().isoformat().replace(":", ".") + ".json"
            with open(outf, 'w') as of:
                json.dump(result, fp=of)

        check_gpu_hang(False)
        Export().export_perf()

    def clean(self):
        pass

    def set_freqs(self):
        """ Run script to fix frequencies (requires properly set up sudoers)"""
        # Prefer scripts in mesa_ci_internal over mesa_perf repo if they exist
        # by leaving mesa_ci_internal location last in this list:
        ff_scripts = [(self._pm.source_root() + '/scripts/' +
                       self.gpu_hw + '_fix_freq'),
                      (self._pm.source_root() +
                       '/repos/mesa_ci_internal/scripts/' + self.gpu_hw +
                       '_fix_freq')
                      ]
        ff_script = None
        for s in ff_scripts:
            if os.path.exists(s):
                ff_script = s
        if not ff_script:
            print("WARN: ***NOT*** fixing frequencies, a script to do so does "
                  "not exist here: " + ff_script)
        else:
            (out, err) = run_batch_command(['sudo', ff_script],
                                           streamedOutput=False, env=self._env)


class AbnPerfTester(PerfTester):
    """
    A PerfTester that uses ABN instead of sixonix

    """

    def __init__(self, benchmark, docker_tag, iterations=2, discard=0,
                 env=None, custom_iterations_fn=None, windowed=False,
                 vk_overlay=False, dxvk=False):
        super().__init__(benchmark, iterations, discard, env,
                         custom_iterations_fn, windowed)
        # Steam and some games in Steam require m32 mesa, while other parts of
        # Steam require m64 mesa. So we include both Mesas in the search paths
        # for libs/drivers
        if self._opt.arch == 'm64':
            prefix = self._build_root.replace('m64', 'm32') + "/lib/"
        else:
            prefix = self._build_root.replace('m32', 'm64') + "/lib/"
        self._env["LD_LIBRARY_PATH"] += ":" + prefix + ":" + prefix + "dri"
        self._env["LIBGL_DRIVERS_PATH"] += ":" + prefix + "dri"
        # make sure games which use SDL don't try to use anything but x11
        self._env["SDL_VIDEODRIVER"] = "x11"
        self._env["VK_ICD_FILENAMES"] = self._build_root + '/share/vulkan/icd.d/intel_icd.x86_64.json'
        # i965 is not being tested anymore in this CI:
        self.steamapps_ver = os.environ.get("STEAMAPPS_VER")
        if not self.steamapps_ver:
            self.steamapps_ver = 'latest'
        self.docker_runner = DockerRunner(docker_tag)
        self.vk_overlay = vk_overlay
        self.dxvk = dxvk

    def build(self):
        # Steam depends on m32 mesa too, so explicitly import it here to
        # build_root
        exporter = Export()
        exporter.import_build_root(arch='m32')

    def test(self):
        save_dir = os.getcwd()
        if self._opt.hardware == "builder":
            print("ERROR: hardware must be set to a specific sku.  'builder' "
                  "is not a valid hardware setting.")
            assert(False)

        abn_dir = glob.glob(self._pm.build_root() + '/opt/abn/ABN-v*')[0]
        os.chdir(abn_dir)
        abn_exe = glob.glob('**/abn_cmd.py', recursive=True)[0]

        self.set_freqs()
        tmp_out_dir = os.path.join(self._build_root, 'tmp', self._benchmark)
        if os.path.exists(tmp_out_dir):
            rmtree(tmp_out_dir)
            os.makedirs(tmp_out_dir)
        interpreter = sys.executable
        driver = self._opt.hardware.split('_')[-1]
        abn_opts = ['-v',
                    '-i', str(self._iterations)]
        if driver == 'vk':
            if self.dxvk:
                abn_opts += ['-s', 'dxvk-g2']
            else:
                abn_opts += ['-s', 'vk-g2']
        else:
            abn_opts += ['-s', 'ogl-g2']

        if self.vk_overlay:
            abn_opts += ['--measure-tools', 'mesavkfpslogger']

        abn_opts += [
            "--storages.json.url", tmp_out_dir,
            "--storages.json.enabled",
            "-t", self._benchmark,
        ]

        # set a timeout for abn that is appropriate. abn will divide the given
        # timeout by the number of iterations, so we set a timeout that is
        # multiplied by the number of iterations. 10 minutes per game iteration
        # should be more than enough...
        # TODO: accept parameter to configure this
        abn_opts += [
            "--timeout", str(10 * self._iterations) + "m"
        ]

        cmd = [interpreter, abn_exe] + abn_opts
        print(" ".join(cmd))
        try:
            (out, err) = self.docker_runner.run(cmd, self._env,
                                                workdir=abn_dir, rm=True)
        except subprocess.CalledProcessError as e:
            # Handle crashed benchmark checking for gpu hang and quitting
            print("ERROR: Benchmark failed!")
            print(e)
            check_gpu_hang(False)
            sys.exit(-1)
        if err:
            print("err: " + err.decode())
        accepted_scores = ['avg fps', 'overall fps']
        score = None
        scores = []
        score_files = []
        for i in range(1, self._iterations + 1):
            if i <= self._discard:
                continue
            score_files += glob.glob(tmp_out_dir
                                     + '/**/*-' + str(i) + '.json')
        if not score_files:
            raise RuntimeError("ERROR: No score files generated!")
        if (len(score_files) < self._iterations) - self._discard:
            raise RuntimeError("ERROR: missing score files for some "
                               "iterations! " + "\n".join(score_files))
        for score_file in score_files:
            try:
                with open(score_file) as f:
                    score_data = json.load(f)
                for scalar in score_data['result']['scalars']:
                    if scalar['metricName'].lower() in accepted_scores:
                        score = scalar['value']
                        break
            except KeyError as e:
                print("ERROR: cannot find score in file " + score_file)
                raise RuntimeError(e)
            except Exception:
                print("ERROR: cannot parse score file in " + tmp_out_dir)
            if not score:
                print("ERROR: unable to find score in " + score_file)
            scores.append(score)
        os.chdir(save_dir)
        # dots are changed to underscores since some downstream components
        # (e.g. javascript) cannot handle it.
        full_bench_name = self._benchmark.replace('.', '_')
        if self._windowed:
            full_bench_name += "_windowed"
        gpu_hw = self._opt.hardware.split("-")[0]
        gen, driver = gpu_hw.split('_')
        result = {
            "benchmark": full_bench_name,
            "project": self._pm.current_project(),
            "gen": gen,
            "driver": driver,
            "mesa_ver": self._mesa_ver,
            "hostname": platform.node(),
            "scores": scores,
            'sys_config': self._sys_config.get(),
            'sys_config_hash': self._sys_config.hash,
            'branchpoint': self._branchpoint,
        }
        out_dir = "/tmp/build_root/" + self._opt.arch + "/scores/" + \
                  full_bench_name + "/" + gpu_hw
        if not os.path.exists(out_dir):
            os.makedirs(out_dir)
        outf = out_dir + "/" + datetime.datetime.now().isoformat().replace(":", ".") + ".json"
        with open(outf, 'w') as of:
            json.dump(result, fp=of)

        check_gpu_hang(False)
        Export().export_perf()
