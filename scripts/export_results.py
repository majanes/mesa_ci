import argparse
import ast
import json
import multiprocessing
import os
import pwd
import shutil
import subprocess
import sys
import tarfile
import tempfile
import datetime

from urllib.parse import urlencode

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                             "..", "build_support"))
from export import abspath2rsync_path
from utils.utils import reliable_url_open

hardware_whitelist = ['bdw', 'bsw', 'builder', 'bxt', 'byt', 'cfl', 'g33',
                      'g45', 'g965', 'glk', 'hsw', 'ilk', 'ivb', 'kbl', 'skl',
                      'snb', 'gen9', 'gen9_zink', 'gen9atom', 'icl', 'icl_zink',
                      'tgl', 'tgl_zink', 'dg2', 'dg2_zink', "xe_gen125", "xe_gen12"]


def serialize_components(local_result_path, build_info, internal=False):
    components = []
    comp_ptr_dir = os.path.join(local_result_path, "component_info_paths")
    default_component = {
        "name": "n/a",
        "status": "n/a",
        "url": "n/a",
        "trigger_time": 0.0,
        "start_time": build_info["start_time"],
        "machine": "n/a",
        "arch": "n/a",
        "shard": "n/a",
        "hardware": "n/a",
        "build": 0,
        "test_files": [],
        "artifacts": [],
        "end_time": build_info["end_time"],
        "collect_time": 0.0
    }

    # populate the list of components by iterating main_invoke build info
    for comp_file in os.listdir(comp_ptr_dir):
        comp_ptr_fpath = os.path.join(comp_ptr_dir, comp_file)
        info_fpath = None
        component = dict(default_component)

        try:
            with open(comp_ptr_fpath, "r") as fh:
                info_fpath = fh.readline().strip()
            with open(os.path.join(local_result_path, info_fpath)) as fh:
                component.update(json.load(fh))

            if internal or (component["hardware"] in hardware_whitelist):
                components += [component]
        except (PermissionError, FileNotFoundError, KeyError) as err:
            print(err)
            print(f"WARN: Failed to serialize component located at {info_fpath}. Skipping...")

            continue

    return components

def get_build_time_info(build_url):
    """return processed start_time, end_time of build"""
    """If no valid start_time and/or end_time return """
    """the current time for both in UTC"""
    now = datetime.datetime.timestamp(datetime.datetime.now())
    time_info = ast.literal_eval(
        reliable_url_open(
            f"{build_url}/api/python?"
            f"{urlencode({'tree': 'timestamp,duration'})}",
            method="POST"
        ).read().decode('utf-8')
    )

    return ((float(time_info["timestamp"]) / 1000) or now,
            (float(time_info["timestamp"] + time_info["duration"]) / 1000) or now)

def get_build_info(local_result_path, internal=False):
    """return a dict containing contents of build_info.json +
    additional information from jenkins build page
    """
    build_info = None

    with open(os.path.join(local_result_path, "build_info.json")) as fh:
        build_info = json.load(fh)

    if not internal:
        build_info["revisions"].pop("mesa_ci_internal")
    build_info["start_time"], build_info["end_time"] = get_build_time_info(build_info["url"])
    build_info["components"] = serialize_components(local_result_path,
                                                    build_info,
                                                    internal=internal)

    return build_info

def create_build_tar(result_path, internal):
    with tempfile.TemporaryDirectory() as local_result_dir:
        os.chdir(local_result_dir)
        # basename(result_path) == "type" option
        local_result_path = os.path.join(local_result_dir,
                                         os.path.basename(os.path.normpath(result_path)))
        build_info = None

        subprocess.check_output(["rsync",
                                 "-r",
                                 "--mkpath",
                                 "--exclude=build_root",
                                 abspath2rsync_path(result_path),
                                 local_result_dir])

        build_info = get_build_info(local_result_path, internal=internal)

        tmpdir = tempfile.mkdtemp()
        try:
            comp_filename = (f"results_{build_info['name']}."
                             f"{build_info['job']}."
                             f"{build_info['build']}.tar").replace("/", "_")
            comp_filepath = os.path.join(tmpdir, comp_filename)
            xz_filepath = comp_filepath + ".xz"

            # limit threads for compression so master doesn't get overwhelmed
            threads = min(16, multiprocessing.cpu_count())
            cmd = ["xz", "-T" + str(threads), comp_filepath]

            with open(os.path.join(tmpdir, "build_info.json"), "w") as fh:
                json.dump(build_info, fh)

            with tarfile.open(comp_filepath, "w") as tf:
                os.chdir(tmpdir)
                tf.add("build_info.json")
                for root, adir, files in os.walk(os.path.normpath(local_result_path + "/test")):
                    os.chdir(root)
                    for afile in files:
                        tf.add(afile)

            subprocess.check_output(cmd)
            print(f"Results successfully compressed to: {xz_filepath}")
        except Exception as err:
            shutil.rmtree(tmpdir)
            raise err

        return xz_filepath


def upload_tar(tar_file):
    scp_cmd = ['scp']
    scp_host = ""
    # Disable host key verification
    scp_cmd.extend(['-o', 'StrictHostKeyChecking=no'])
    scp_cmd.extend(['-o', 'UserKnownHostsFile=/dev/null'])
    user = pwd.getpwuid(os.getuid()).pw_name
    if "MESA_CI_RESULT_HOST_PORT" in os.environ:
        scp_port = os.environ["MESA_CI_RESULT_HOST_PORT"]
        scp_cmd.extend(['-P', scp_port])
    scp_keyfile = None
    if "MESA_CI_SSH_KEYFILE" in os.environ:
        scp_keyfile = os.path.expanduser(os.environ["MESA_CI_SSH_KEYFILE"])
        scp_cmd.extend(['-i', scp_keyfile])
    scp_dest = '/tmp/mesa_ci_results'
    if "MESA_CI_RESULT_HOST_PATH" in os.environ:
        scp_dest = os.environ["MESA_CI_RESULT_HOST_PATH"]
    if "MESA_CI_RESULT_USER" in os.environ:
        user = os.environ["MESA_CI_RESULT_USER"]
        scp_host = user + "@"
    host = 'localhost'
    if "MESA_CI_RESULT_HOST" in os.environ:
        host = os.environ["MESA_CI_RESULT_HOST"]
    scp_cmd.append(tar_file)
    scp_host += host
    scp_dest = '%s:%s' % (scp_host, scp_dest)
    scp_cmd.append(scp_dest)
    subprocess.check_output(scp_cmd)


parser = argparse.ArgumentParser(description=("Exports test data to a remote"
                                              " system for importing into SQL"))
parser.add_argument('result_path',
                    type=str,
                    help="remote result path for build (eg, "
                    "/mnt/space/results/dev_ngcortes/"
                    "e9b385103a2c31fc8d49d63e0ea6c123/percheckin")

parser.add_argument('--internal', action='store_true',
                    help=('Include hardware that is not in the hardware '
                          'whitelist'))

parser.add_argument('--all_builds', action='store_true',
                    help=('Import all builds from job path '
                          'NB: result path must be of form: '
                          '/mnt/space/results/dev_ngcortes/'))

opts = parser.parse_args()
build_dirs = [opts.result_path]

if opts.all_builds:
    with tempfile.TemporaryDirectory() as tempd:
        os.chdir(tempd)
        temp_job_path = os.path.join(tempd,
                                     os.path.basename(os.path.normpath(opts.result_path)))
        subprocess.check_output(['rsync',
                                 '-r',
                                 '--exclude=/*/*/*/*/',
                                 abspath2rsync_path(opts.result_path),
                                 tempd])

        build_dirs = filter(lambda path: path != opts.result_path,
                            map(lambda subsubd: os.path.normpath(os.path.join(opts.result_path, subsubd)),
                                #list out all relative subdirs to the job's result path; exclude everything else.
                                subprocess.check_output(['find',
                                                         temp_job_path,
                                                         '-mindepth',  '2',
                                                         '-maxdepth', '2',
                                                         '-type', 'd',
                                                         '-printf', '%P\n'],
                                                        text=True).split("\n")))

for d in build_dirs:
    print(f"Building tarfile for {d}...")
    tar_file = create_build_tar(d, opts.internal)

    upload_tar(tar_file)
    shutil.rmtree(os.path.normpath(tar_file + "/.."))

sys.exit(0)
