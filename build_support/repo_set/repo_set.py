# Copyright (C) Intel Corp.  2018.  All Rights Reserved.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  **********************************************************************/
#  * Authors:
#  *   Mark Janes <mark.a.janes@intel.com>
#  *   Clayton Craft <clayton.a.craft@intel.com>
#  **********************************************************************/

import glob
import hashlib
import json
import os
import random
import re
import shutil
import signal
import subprocess
import sys
import threading
import time
from urllib.request import HTTPError
# TODO: cElementTree is deprecated
import xml.etree.cElementTree as et

import git

from project_map import ProjectMap
from project_map import BuildSpec
from options import Options
from utils.utils import reliable_url_open
from utils.command import run_batch_command


external_to_repo = {"spirv-headers" : "spirvheaders",
                    "spirv-tools" : "spirvtools",
                    "vulkan-docs" : "vulkandocs",
                    "amber" : "amber",
                    "googletest" : "gtest",
                    "common" : "google_common",
                    "buildtools" : "google_buildtools",
                    "video-parser" : "vk-video-parser",
                    "ESExtractor" : "esextractor",
                    "nvidia-video-samples": "vk-video-parser"
}
repo_to_external = {v: k for (k, v) in external_to_repo.items()}

def apply_patches(project_dir, source_dir):
    """apply patches if they exist"""

    save_dir = os.getcwd()
    git_env = {"GIT_AUTHOR_NAME" : "Mesa CI",
               "GIT_AUTHOR_EMAIL" : "mesa_ci@intelc.com",
               "GIT_COMMITTER_NAME" : "Mesa CI",
               "GIT_COMMITTER_EMAIL" : "mesa_ci@intelc.com",
               "EMAIL" : "mesa_ci@intelc.com" }
    for patch in sorted(glob.glob(f"{project_dir}/*.patch")):
        os.chdir(source_dir)
        try:
            run_batch_command(["git", "am", patch], env = git_env)
        except:
            print("WARN: failed to apply patch: " + patch)
            run_batch_command(["git", "am", "--abort"], env = git_env)
    os.chdir(save_dir)

def checkout_externals(project, revisions, external_dir_format):
    pm = ProjectMap()
    for external_repo, rev in revisions.items():
        if isinstance(rev, list):
            # revision may be passed in a list, but we can check out
            # exactly one commit.
            rev = rev[0]

        external_dir = external_dir_format.format(external_repo)
        external_proj = external_repo
        if external_repo in repo_to_external:
            external_proj = repo_to_external[external_repo]
            external_dir = external_dir_format.format(external_proj)
        print("Checking out external: {}".format(external_proj))
        if not os.path.exists(external_dir):
            # clone the project from the local cache in repos
            git.Repo.clone_from(pm.project_source_dir(external_repo),
                                external_dir)
            repo = git.Repo(external_dir)

        savedir = os.getcwd()
        os.chdir(external_dir)
        run_batch_command(["git", "clean", "-xfd"])
        run_batch_command(["git", "reset", "--hard", "HEAD"])
        os.chdir(savedir)
        repo = git.Repo(external_dir)
        fetch_tag = None
        if _is_tag_name(rev):
            fetch_tag="tag"
        try:
            repo.commit(rev)
        except:
            repo.git.fetch("--depth=10", "origin", fetch_tag, rev)
        repo.git.checkout(rev, force=True)

class RevisionSpecification:
    _build_lab = None
    def __init__(self, revisions=None, repo_set=None, only_projects=None, project_map=None):
        # key is project, value is revision
        if revisions is not None:
            assert isinstance(revisions, dict)
            self._revisions = revisions
        else:
            self._revisions = {}
            if repo_set is None:
                repo_set = RepoSet(project_map=project_map)
            # By default, take all projects
            if only_projects is None:
                only_projects = repo_set.projects()
            for p in only_projects:
                try:
                    repo = repo_set.repo(p)
                    rev = repo.git.rev_parse("HEAD", short=False)
                except:
                    continue
                self._revisions[p] = rev

    @classmethod
    def from_xml_file(cls, filename):
        elem = et.ElementTree(file=filename).getroot()
        if elem.tag != 'RevSpec':
            elem = elem.find('RevSpec')
            assert elem is not None
        inst = cls(revisions=elem.attrib)
        return inst

    @classmethod
    def from_cmd_line_param(cls, params):
        return cls(revisions=dict(p.split('=') for p in params))

    def to_cmd_line_param(self):
        revs = []
        for project, revision in self._revisions.items():
            if type(revision) is dict:
                revision = revision['sha']
            revs.append(project + "=" + revision)
        revs.sort()
        return " ".join(revs)

    def to_elementtree(self):
        elem = et.Element('RevSpec')
        for n, h in sorted(self._revisions.items(), key=lambda x: x[0]):
            elem.set(n, h)
        return et.ElementTree(elem)

    def __str__(self):
        return et.tostring(self.to_elementtree().getroot(),
                           encoding="UTF-8").decode()

    def checkout(self):
        repo_set = RepoSet()
        for (project, revision) in self._revisions.items():
            project_repo = repo_set.repo(project)
            project_repo.git.checkout(["-f", revision])

    def revision(self, project):
        return self._revisions[project]

    def set_revision(self, project, rev):
        assert(project in self._revisions)
        self._revisions[project] = rev

    def projects(self):
        return self._revisions.keys()

    def get_branchpoint(self, project, trunk="origin/master"):
        """ Get the branch point for the currently checked out revision in the
        given project, in the given trunk 'branch' (defaults to
        'origin/master') of the project.
        Returns None if unable to find one.
        """
        mesa_repo = git.Repo(ProjectMap().project_source_dir("mesa"))
        branch = self._revisions[project]
        bp = None
        try:
            bp = mesa_repo.git.merge_base(trunk, branch)
        except git.exc.GitCommandError as e:
            print(e)
            print("ERROR: unable to retrieve branch point between "
                  f" {trunk} and {branch}")
        return bp

class TimeoutException(Exception):
    def __init__(self, msg):
        self._msg = msg

    def __str__(self):
        return self._msg

def is_build_lab():
    if RevisionSpecification._build_lab is not None:
        return RevisionSpecification._build_lab
    buildspec = ProjectMap().build_spec()
    master_host = buildspec.master_hostname()
    # Sometimes the lab network is under load, give adequate time for a ping
    # response
    cmd = ["ping", "-c", "1", "-w", "5", "-q", master_host + ".local"]
    if os.name == "nt":
        master_host = buildspec.master_host()
        cmd = ["ping", "-w", "5", master_host]
    p = subprocess.Popen(cmd,
                         stderr=open(os.devnull, "w"), stdout=open(os.devnull, "w"))
    p.communicate()
    if p.returncode:
        # error from ping: not in build lab
        RevisionSpecification._build_lab = False
    else:
        RevisionSpecification._build_lab = True
    return RevisionSpecification._build_lab


class RepoNotCloned(Exception):
    def __init__(self, repo):
        Exception.__init__(self, "Repo should be cloned first: %s" % repo)


def _is_tag_name(ref):
    if bool(re.findall(r"([a-fA-F\d]{32})", ref)):
        # this is a md5sum
        return False
    if ':' in ref:
        # this is refspec
        return False
    if '/' in ref:
        # this is a remote/branch name
        return  False
    return True

class RepoSet:
    """this class represents the set of git repositories which are
    specified in the build_specification.xml file."""
    def __init__(self, repos_root=None, use_cache=True, mirror=False, project_map=None):
        """
        Keyword arguments:
        repos_root  -- Destination for git repositories (default is ./repos/)
        use_cache   -- Cloning/fetching will happen from
                       build_master (default True)
        mirror      -- Pass --mirror when creating clones (default False)
        """
        if project_map:
            self.project_map = project_map
        else:
            self.project_map = ProjectMap()
        self.buildspec = self.project_map.build_spec()
        self._repos = {}
        # key is repo name, value is the default branch for the repo
        # (usually master)
        self._branches = {}
        self._use_cache = use_cache
        self._build_lab = is_build_lab()

        self._repos_root = self.project_map.source_root() + "/repos/"
        if repos_root:
            self._repos_root = repos_root

        # Validate any existing repos under repos_root and add them to
        # this object
        repos = self.buildspec.xml().find("repos")
        for tag in repos:
            project = tag.tag
            project_repo_dir = self._repos_root + "/" + project
            if os.path.exists(project_repo_dir) and not os.path.exists(f"{project_repo_dir}/do_not_use"):
                try:
                    repo = git.Repo(project_repo_dir)
                    self._repos[project] = repo
                    branch = "origin/master"
                    if "branch" in tag.attrib:
                        branch = tag.attrib["branch"]
                    self._branches[project] = branch
                except git.InvalidGitRepositoryError:
                    # Something broke with the repo, so remove it and re-clone
                    print("INFO: Repo path is not a valid git repo: %s. Removing..."
                          % project_repo_dir)
                    shutil.rmtree(project_repo_dir)

    def clone(self, limit_to_repos=None):
        """ Clone all repos specified in build_specification.xml
            Note: This method does *not* fetch remotes """
        cloned_repo = False
        if not os.path.exists(self._repos_root):
            os.makedirs(self._repos_root)
        attempts = 3
        if self._build_lab:
            attempts = 10
        # Parse buildspec in case there were changes since RepoSet
        # was initialized
        self.buildspec = ProjectMap().build_spec()

        repos = self.buildspec.xml().find("repos")
        # clone all the repos into repos_root
        for tag in repos:
            repo_name = tag.tag
            repo_dir = self._repos_root + tag.tag
            if os.path.exists(repo_dir + "/do_not_use"):
                continue
            if limit_to_repos and repo_name not in limit_to_repos:
                continue

            # Builders/testers should clone from master's cache,
            # everything else will clone from upstream.
            url = self.url_for_remote(repo_name, "origin")
            branch = self.buildspec.default_branch_for_repo(repo_name)
            # Try to use existing repo_dir if there is one. If it's invalid
            # and not explicitly disabled, then remove it so that a re-clone
            # can be attempted
            repo = None
            if os.path.exists(repo_dir):
                try:
                    repo = git.Repo(repo_dir)
                except git.InvalidGitRepositoryError:
                    # Something broke with the repo, so remove it and re-clone
                    print("INFO: Repo path exists but is not a valid git "
                          f"repo: {repo_dir}. Attempting to repair... ")
                    shutil.rmtree(repo_dir)
            # Clone any repos that do not exist on disk
            if not os.path.exists(repo_dir):
                os.makedirs(repo_dir)
                success = False
                clone_branch = "/".join(branch.split("/")[1:])
                clone_repo = branch.split("/")[0]
                age = 1
                for attempt in range(0, attempts):
                    if attempt > 0:
                        time.sleep(1)
                    try:
                        print("Attempting clone of %s" % url)
                        repo = git.cmd.Git(repo_dir)
                        if clone_repo == "origin":
                            repo.clone(f"--shallow-since='{str(age)} year ago'",
                                       f"--branch={clone_branch}",
                                       url, repo_dir)
                        else:
                            repo.clone(f"--shallow-since='{str(age)} year ago'",
                                       url, repo_dir)
                        success = True
                        cloned_repo = True
                        break
                    except git.GitCommandError as e:
                        if ("shallow" not in str(e) and
                            "the remote end hung up unexpectedly" not in str(e)):
                            print(f"WARN: unable to clone repo: {url}\nException text: {str(e)}")
                        else:
                            age = age + 1
                # If the repo is not clone-able, do_not_use is used to disable
                # it from any future attempts to clone/fetch
                if not success and not self._build_lab:
                    os.makedirs(repo_dir + "/do_not_use")
                    continue
            if os.path.exists(repo_dir + "/do_not_use"):
                continue
            try:
                repo = git.Repo(repo_dir)
            except git.InvalidGitRepositoryError:
                if not self._build_lab:
                    os.makedirs(repo_dir + "/do_not_use")
                print("WARNING: Unable to clone repo: %s" % repo_name)

            # Store repo, branch, and remote object(s)
            self._repos[repo_name] = repo
            self._branches[repo_name] = branch
        return cloned_repo

    def repo(self, project_name):
        return self._repos[project_name]

    def branch(self, project_name):
        return self._branches[project_name]

    def projects(self):
        return self._repos.keys()

    def alarm(self, secs):
        if os.name != "nt":
            signal.alarm(secs)   # 5 minutes

    def url_for_remote(self, repo, remote):
        """Return the upstream url that would need to be fetched for the given repo/remote pair."""
        if self._build_lab:
            master = self.buildspec.git_hostname()
            return f"git://{master}.local/git/mirror/{repo}/{remote}"
        if remote == "origin":
            return self.buildspec.xml().find(f"repos/{repo}").attrib["repo"]
        for remote_tag in self.buildspec.xml().findall(f"repos/{repo}/remote"):
            if remote == remote_tag.attrib["name"]:
                return remote_tag.attrib["repo"]
        print(f"Error: bad remote {repo}/{remote}")
        assert False

    def has_ref(self, project, commit_sha):
        """Check to see if the target sha already exists in the repo. in this
        case, no fetch is required.
        """
        try:
            self._repos[project].commit(commit_sha)
            # already fetched the specific rev
            return True
        except:
            return False

    def fetch_target_shallow(self, repo, url, target):
        intervals = ["1 year ago",
                     "2 years ago",
                     "4 years ago",
                     "8 years ago",
                     ]
        fetch_tag = None
        if _is_tag_name(target):
            fetch_tag="tag"

        for age in intervals:
            try:
                repo.git.fetch("-f",
                               "--no-tags",
                               f"--shallow-since='{age}'",
                               "--update-head-ok",
                               url, fetch_tag, target)
                return True
            except:
                pass
        return False

    def fetch_project(self, project, remote_and_branch):
        """Fetch the targeted commit for the project"""
        repo = self._repos[project]
        if '/' not in remote_and_branch:
            # we are asked to get a sha, but we do not know which remote it is from
            target_sha = remote_and_branch
            # first check to see if the sha is already in the repo
            if self.has_ref(project, target_sha):
                return True

            # build a list of url that could contain the sha
            urls = [self.url_for_remote(project, "origin")]
            for remote_tag in self.buildspec.xml().findall(f"repos/{project}/remote"):
                urls.append(self.url_for_remote(project, remote_tag.attrib["name"]))

            for url in urls:
                if self.fetch_target_shallow(repo, url, target_sha):
                    return True
            if self.has_ref(project, target_sha):
                print("fetch_target_shallow returned False but commit was fetched")
                assert False
            print(f"Error: could not fetch {target_sha} from {project}")
            sys.exit(-1)

        # else we are fetching a branch
        components = remote_and_branch.split('/')
        remote = components[0]
        branch = '/'.join(components[1:])
        refspec = f"refs/heads/{branch}:refs/heads/{remote}/{branch}"
        url = self.url_for_remote(project, remote)
        if not self.fetch_target_shallow(repo, url, refspec):
            print(f"Error: could not fetch {project}/{remote}/{branch}")
            sys.exit(-1)

    def tidy_project(self, project, gcoll):
        """cleans up git references and detects errors in a repo"""
        repo = self._repos[project]
        try:
            repo.git.repack("-d")
        except Exception as e:
            print(f"WARN: could not repack {project}: {str(e)}")
        # Removing gc.log is relevant only for systems where the repo
        # is not bare:
        if repo.working_tree_dir:
            garbage_collection_fail = (repo.working_tree_dir +
                                       "/.git/gc.log")
            if os.path.exists(garbage_collection_fail):
                run_batch_command(["rm", "-f", garbage_collection_fail])
        if gcoll:
            print(f"calling git gc on: {project}")
            try:
                repo.git().gc()
            except Exception as e:
                print(f"ERROR: git repo is corrupt, removing: {repo.working_tree_dir}")
                print(e)
                run_batch_command(["rm", "-rf", repo.working_tree_dir])
                raise
    
    def fetch(self, only_projects, gc=False):
        """iterate through projects to collect the requested commits"""
        def signal_handler(signum, frame):
            raise TimeoutException("Fetch timed out.")

        for project, remote_and_branch in only_projects.items():
            if project not in self._repos:
                print(f"Skipping fetch of {project}")
                continue

            self.tidy_project(project, gc)

            # fetch the default branch, for checking missing revisions
            default_branch = self._branches[project]
            self.fetch_project(project, default_branch)
            if remote_and_branch != default_branch:
                # fetch specified branch or sha
                self.fetch_project(project, remote_and_branch)

    def branch_missing_revisions(self, deps=None):
        """provides the revisions which are on master but are not on the
        current branch(es).  This information can be used to filter
        out known test failures that only exist on the branch

        """
        if deps:
            projects = deps.all_sources()
        else:
            projects = self.projects()
        revs = []
        for project in projects:
            repo = self.repo(project)
            try:
                default_branch = f"{self._branches[project]}"
                merge_base = repo.git.merge_base("HEAD", default_branch)
                revs = revs + repo.git.rev_list(f"{merge_base}..{default_branch}").split()
            except:
                print(f"Warning: Unable to find branch commits in {project}")
                continue

        return revs

    def checkout(self, revision_dict):
        for (repo_name, revision) in revision_dict.items():
            if not revision:
                continue
            if repo_name not in self._repos:
                print(f"Skipping checkout of {repo_name}")
                continue
            repo = self._repos[repo_name]
            lock_file = repo.working_tree_dir + "/.git/index.lock"
            if os.path.exists(lock_file):
                run_batch_command(["rm", "-f", lock_file])
            repo.git.reset("--hard")
            repo.git.checkout(["-f", revision])

    def serializable_details(self):
        """ Return formatted details for all
        checked out commits (eg. when
        exporting results to the results server)
        """
        ret = {}

        for k,v in self._repos.items():
            ret[k] = {
                "commit": f"{k}={v.head.commit.hexsha}",
                "author": v.head.commit.author.name,
                "description": v.head.commit.summary,
                "sha": str(v.head.commit.hexsha)
            }

        return ret


class LastBuild:
    """Tracks the sha used for the last build that was triggered"""
    def __init__(self, mirror, branch):
        self._mirror = mirror
        self._branch = branch

        # get sha from branch HEAD
        self._sha = mirror._repo.commit(branch).hexsha

        # list of builds to trigger
        self._projects = []

        # populate self._projects from buildspec
        buildspec = ProjectMap().build_spec()
        self._server = buildspec.master_host()

        for branch_tag in buildspec.xml().findall("branches/branch"):
            repo_tag = branch_tag.find(mirror._repo_name)
            if repo_tag is None:
                continue
            if "branch" not in repo_tag.attrib:
                continue
            branch = repo_tag.attrib["branch"]
            branch_components = branch.split('/')
            remote = branch_components[0]
            if remote != mirror._remote_name:
                continue
            branch_name = '/'.join(branch_components[1:])
            if branch_name != self._branch:
                continue
            try:
                branch_pipeline = branch_tag.attrib["pipeline"]
            except:
                branch_pipeline = ""
            self._projects.append({"name": branch_tag.attrib["name"], "pipeline": branch_pipeline})

    def poll(self):
        """Trigger a build if project has been updated, and update the last sha"""
        # builds the projects if the sha is new
        new_sha = self._mirror._repo.commit(self._branch).hexsha
        if new_sha == self._sha:
            return

        # Can't access jenkins master directly if proxy is set
        os.environ["http_proxy"] = ""

        # update the sha
        self._sha = new_sha
        shortrev = self._mirror._repo.git.rev_parse(new_sha, short=True)
        for project in self._projects:
            sys.stderr.flush()

            full_project_name = project["name"]
            token_param = "token=noauth&"
            if project["pipeline"]:
                full_project_name = project["pipeline"] + "/job/" + project["name"]
                token_param = ""
            print("Building " + full_project_name, file=sys.stderr)
            job_url = (f"http://{self._server}/job/{full_project_name}/buildWithParameters?"
                       f"{token_param}"
                       f"name={self._mirror._repo_name}_{shortrev}&type=percheckin")

            retry_count = 0

            while retry_count < 10:
                try:
                    fileh = reliable_url_open(job_url, method="POST")
                    fileh.read()
                    fileh.close()
                    break
                except HTTPError as url_error:
                    print(url_error, file=sys.stderr)
                    retry_count = retry_count + 1
                    print("ERROR: failed to reach jenkins, retrying: " +
                          job_url, file=sys.stderr)
                    sys.stderr.flush()
                    time.sleep(10)

class Mirror:
    def __init__(self, repo_name, remote_name, path, url):
        self._repo_name = repo_name
        self._remote_name = remote_name
        self._path = path
        self._repo = None
        self._remote = None
        self._url = url

        if (os.path.exists(path) and
            not os.path.exists(os.path.join(path, "auth_error"))):
            try:
                self._repo = git.Repo(path)
                self._remote = self._repo.remote("origin")
            except git.exc.InvalidGitRepositoryError:
                print(f"Error - invalid repo: {path}")
                self._repo = None
                self._remote = None

        # key: branch
        # value: LastBuild
        self._last_build = {}

    def clone(self):
        if self._repo:
            # already cloned
            return
        if os.path.exists(os.path.join(self._path, "auth_error")):
            return
        if os.path.exists(self._path):
            # invalid repo
            shutil.rmtree(self._path)

        os.makedirs(self._path)
        repo = git.cmd.Git(self._path)
        if not self._path.endswith("origin"):
            # add a reference to origin, to save network/disk
            origin_path = os.path.split(self._path)[0] + "/origin"
            if os.path.exists(origin_path):
                try:
                    repo.clone("--bare", "--mirror",
                               "--reference", origin_path,
                               self._url, self._path)
                    run_batch_command(['touch',
                                       self._path + '/git-daemon-export-ok'])
                    self._repo = git.Repo(self._path)
                    self._remote = self._repo.remote("origin")
                except Exception as clone_error:
                    print(clone_error)
                    run_batch_command(['touch',
                                       self._path + '/auth_error'])
            return
        # else
        try:
            repo.clone("--bare", "--mirror", self._url, self._path)
            run_batch_command(['touch',
                               self._path + '/git-daemon-export-ok'])
            self._repo = git.Repo(self._path)
            self._remote = self._repo.remote("origin")
        except Exception as clone_error:
            print(clone_error)
            run_batch_command(['touch',
                               self._path + '/auth_error'])

    def init_branches(self):
        """use the buildspec to establish the current commit at the HEAD of each branch
        that triggers CI projects"""
        build_spec = ProjectMap().build_spec()
        branches = build_spec.watched_branches_in_remote(self._repo_name, self._remote_name)
        for branch_name in branches:
            self._last_build[branch_name] = LastBuild(self, branch_name)

    def fetch(self):
        if not self._repo:
            return
        try:
            self._remote.fetch()
        except Exception as unknown_error:
            print(f"Error: could not fetch git repo: {self._repo_name}/{self._remote_name} {unknown_error}")
        for last in self._last_build.values():
            last.poll()

def clone_thread(mirror):
    """dispatch method for threading"""
    start_time = time.time()
    mirror.clone()
    total_time = time.time() - start_time
    print(f"{mirror._path} cloned in {total_time}")

def fetch_thread(mirror):
    while True:
        start_time = time.time()
        mirror.fetch()
        elapsed = time.time() - start_time
        print(f"fetched {mirror._path} in {elapsed}s")
        time.sleep(random.random() * 30 + 10)

class Mirrors:
    def __init__(self, path="/var/lib/git/mirror"):
        buildspec = ProjectMap().build_spec().xml()
        self._mirrors = []
        threads = []
        repos_tag = buildspec.find("repos")

        # first clone the origins, so remotes can use them as references
        for repo in repos_tag:
            repo_name = repo.tag
            origin_url = repo.attrib["repo"]
            repo_dir = os.path.join(path, repo_name, "origin")
            mirror = Mirror(repo_name, "origin", repo_dir, origin_url)
            self._mirrors.append(mirror)
            new_thread = threading.Thread(group=None,
                                          target=clone_thread,
                                          args=(mirror, ),
                                          name=repo_dir)
            new_thread.start()
            threads.append(new_thread)
        for running_thread in threads:
            running_thread.join()

        threads = []
        for repo in repos_tag:
            repo_name = repo.tag
            for remote in repo.findall("remote"):
                remote_url = remote.attrib["repo"]
                remote_name = remote.attrib["name"]
                remote_dir = os.path.join(path, repo_name, remote_name)
                mirror = Mirror(repo_name, remote_name,
                                remote_dir, remote_url)
                self._mirrors.append(mirror)
                new_thread = threading.Thread(group=None,
                                              target=clone_thread,
                                              args=(mirror, ),
                                              name=repo_dir)
                new_thread.start()
                threads.append(new_thread)
        for running_thread in threads:
            running_thread.join()

        for mirror in self._mirrors:
            mirror.init_branches()

    def poll(self):
        threads = []
        for mirror in self._mirrors:
            try:
                print(f"starting {mirror._path}")
                poll_thread = threading.Thread(group=None,
                                               target=fetch_thread,
                                               args=(mirror,),
                                               name=mirror._path)
                poll_thread.start()
                threads.append(poll_thread)
            except(git.exc.InvalidGitRepositoryError):
                print(f"invalid repo: {mirror._path}")
