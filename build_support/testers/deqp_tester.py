#!/usr/bin/env python3
import bz2
import datetime
import glob
import importlib
import os
import pickle
import shutil
import subprocess
import sys
import tempfile
import time
import xml.etree.cElementTree as et
import xml.sax.saxutils as saxutils
import socket

from dependency_graph import DependencyGraph
from utils.utils import (mesa_version, get_libdir, get_libgl_drivers,
                         is_soft_fp64, get_blacklists,
                         Flakes)
from utils.check_gpu_hang import check_gpu_hang
from utils.command import run_batch_command
from utils.utils import cpu_count
from export import Export, fail_and_reboot
from options import Options
from project_map import ProjectMap
from bisect_test import CaseConfig, TestLister
from repo_set import RepoSet, external_to_repo
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                             "../../..", "mesa_ci_internal"))
try:
    import internal_build_support.vars as internal_vars
except ModuleNotFoundError:
    internal_vars = None


class TestResult:
    def __init__(self):
        self._result = None
        self._duration = None
        self._stdout = None
        self._stderr = None

    def result(self, value=None):
        if value:
            self._result = value
        if self._result is None:
            self._result = ""
        return self._result

    def stdout(self, value=None):
        if value:
            self._stdout = value
        if self._stdout == None:
            self._stdout = ""
        return self._stdout

    def stderr(self, value=None):
        if value:
            self._stderr = value
        if self._stderr == None:
            self._stderr = ""
        return self._stderr
    
    def duration(self, value=None):
        if value:
            self._duration = value
        if self._duration == None:
            self._duration = 0.0
        return self._duration
    
class DeqpTrie:
    def __init__(self, trie=None, runtime=None):
        assert runtime is None
        if trie is not None:
            self._trie = trie
        else:
            self._trie = {}
        self._result = {}
        if '__result__' in self._trie:
            self._result = self._trie['__result__']

    def result(self, test):
        if '__result__' not in self._trie:
            self._trie['__result__'] = {}
        self._result = self._trie['__result__']
        result = self._result
        if test not in result:
            result[test] = TestResult()
        return result[test]

    def results(self):
        return list(self._result.items())

    def empty(self):
        # TODO: remove assert or len test, depending on what is needed
        assert '__result__' not in self._trie
        if len(self._trie) == 1:
            assert '__result__' not in self._trie
        return not self._trie

    def results_count(self, running_count=0):
        for test, subtrie in self._trie.items():
            if test == '__result__':
                continue
            running_count = DeqpTrie(subtrie).results_count(running_count)
        if '__result__' not in self._trie:
            return running_count
        running_count += len(self._result)
        return running_count

    def test_count(self, running_count=None):
        trie = self._trie
        if not trie:
            if running_count is None:
                return 0
            # else
            return running_count + 1
        if running_count is None:
            running_count = 0
        for test, subtrie in self._trie.items():
            if test == '__result__':
                continue
            running_count = DeqpTrie(subtrie).test_count(running_count)
        return running_count

    def add_txt(self, txt_file):
        fh = None
        if (txt_file[-4:] == ".bz2"):
            fh = bz2.BZ2File(txt_file)
        else:
            fh = open(txt_file)

        for line in fh.readlines():
            line = line.strip()
            # Ignore comment lines
            if not line.startswith('#'):
                self.add_line(line)

    def add_line(self, line):
        self._add_split_line(line.split("."))
            
    def _add_split_line(self, line):
        if not line:
            return
        group = line[0]
        if line == "performance":
            return
        if not group in self._trie:
            self._trie[group] = {}
        DeqpTrie(self._trie[group])._add_split_line(line[1:])
            
    def add_xml(self, xml_file):
        current_trie = {}
        if "KHR-GLES2-cases" in xml_file:
            self._trie["KHR-GLES2"] = current_trie
        elif "KHR-GLES3-cases" in xml_file:
            self._trie["KHR-GLES3"] = current_trie
        elif "KHR-GLES31-cases" in xml_file:
            self._trie["KHR-GLES31"] = current_trie
        elif "KHR-GLES32-cases" in xml_file:
            self._trie["KHR-GLES32"] = current_trie
        elif "KHR-GLESEXT-cases" in xml_file:
            self._trie["KHR-GLESEXT"] = current_trie
        elif "dEQP-GLES2-cases" in xml_file:
            self._trie["dEQP-GLES2"] = current_trie
        elif "dEQP-GLES3-cases" in xml_file:
            self._trie["dEQP-GLES3"] = current_trie
        elif "dEQP-GLES31-cases" in xml_file:
            self._trie["dEQP-GLES31"] = current_trie
        elif "dEQP-VK-cases" in xml_file:
            self._trie["dEQP-VK"] = current_trie
        elif "dEQP-EGL-cases" in xml_file:
            self._trie["dEQP-EGL"] = current_trie
        elif "CTS-Configs-cases" in xml_file:
            self._trie["CTS-Configs"] = current_trie
        elif "ES2-CTS-cases" in xml_file:
            self._trie["ES2-CTS"] = current_trie
        elif "ES3-CTS-cases" in xml_file:
            self._trie["ES3-CTS"] = current_trie
        elif "ES31-CTS-cases" in xml_file:
            self._trie["ES31-CTS"] = current_trie
        elif "ES32-CTS-cases" in xml_file:
            self._trie["ES32-CTS"] = current_trie
        elif "ESEXT-CTS" in xml_file:
            self._trie["ESEXT-CTS"] = current_trie
        elif "KHR-GL33" in xml_file:
            self._trie["KHR-GL33"] = current_trie
        elif "KHR-GL46" in xml_file:
            self._trie["KHR-GL46"] = current_trie
        elif "GTF-GL33" in xml_file:
            self._trie["GTF-GL33"] = current_trie
        elif "GTF-GL46" in xml_file:
            self._trie["GTF-GL46"] = current_trie
        elif "KHR-NoContext" in xml_file:
            self._trie["KHR-NoContext"] = current_trie
        else:
            return
        root = et.parse(xml_file).getroot()
        for tag in root:
            DeqpTrie(current_trie)._add_tag(tag)
        del root

    def _add_tag(self, tag):
        name = tag.attrib["Name"]
        if name == "performance":
            return
        if not name in self._trie:
            self._trie[name] = {}

        for child in tag:
            DeqpTrie(self._trie[name])._add_tag(child)

    def _filter(self, blacklist):
        ### recursive step
        if not blacklist._trie and self._trie:
            # caller is filtering out a group of tests with a common
            # prefix.
            self._trie = {}
        for group in list(blacklist._trie.keys()):
            if group == '__result__':
                continue
            if group not in self._trie:
                continue
            group_trie = DeqpTrie(self._trie[group])
            group_trie._filter(DeqpTrie(blacklist._trie[group]))
            if group_trie.empty():
                del self._trie[group]

    def filter(self, blacklist):
        # blacklist can either be a trie or a list of tests
        if list == type(blacklist):
            if not blacklist:
                return
            bltrie = DeqpTrie()
            for test in blacklist:
                bltrie.add_line(test)
            blacklist = bltrie
        if blacklist.test_count() == 0:
            return
        self._filter(blacklist)

    def matching_tests(self, wildcard, prefix="", matches=None):
        _matches = matches
        next_prefix = f"{prefix}."
        if _matches is None:
            _matches = []
            wildcard = wildcard.split('.')
            next_prefix = ""
        if not wildcard:
            wildcard = ['*']
        group = wildcard.pop(0)
        if group == '*':
            if self.empty():
                _matches.append(prefix)
                return _matches
            for group, trie in self._trie.items():
                if group == '__result__':
                    continue
                DeqpTrie(trie).matching_tests(['*'], f"{next_prefix}{group}", _matches)
            return _matches

        if group not in self._trie:
            return _matches
        return DeqpTrie(self._trie[group]).matching_tests(wildcard, f"{next_prefix}{group}", _matches)

    def matching_tests_for_files(self, files):
        """for a list of blacklist or whitelist files, return a list of all
        tests which match the test groups identified."""
        if not isinstance(files, list):
            files = [files]
        match_lines = set()
        for file in files:
            with open(file, encoding="utf8") as listfileh:
                for line in listfileh.readlines():
                    line = line.strip()
                    if line.startswith("#"):
                        continue
                    if not line:
                        continue
                    match_lines.add(line)
        matched_tests = set()
        for match_line in match_lines:
            matched_tests.update(self.matching_tests(match_line))
        return sorted(matched_tests)

    def filter_whitelist(self, whitelist, prefix=""):
        if "*" in whitelist._trie:
            return
        for group in list(self._trie.keys()):
            if group == '__result__':
                assert False
                continue
            if group not in whitelist._trie:
                del (self._trie[group])
                continue
            group_trie = DeqpTrie(self._trie[group])
            wl_group_trie = DeqpTrie(whitelist._trie[group])
            group_trie.filter_whitelist(wl_group_trie, prefix=prefix + group + ".")

    def pop_front(self, prefix=""):
        # get rid of single-core testing
        assert False
        items = list(self._trie.items())
        items.sort()
        # can't pop an empty list
        assert(items)
        group, trie = items[0]
        if len(trie._trie) == 0:
            del self._trie[group]
            return(prefix + "." + group)
        # else
        if not prefix:
            prefix = group
        else:
            prefix = prefix + "." + group
        case_name =  trie.pop_front(prefix)
        if trie.empty():
            del self._trie[group]
        return case_name

    def write_caselist(self, outfh, prefix="", shard=0, shard_count=0, current_shard=1):
        items = list(self._trie.items())
        # ensure stable order, so sharding will work correctly
        items.sort()
        for group, trie in items:
            assert group != '__result__'
            if DeqpTrie(trie).empty():
                # this is a test, not a group
                if shard in (0, current_shard):
                    outfh.write(prefix + "." + group + "\n")
                if shard:
                    current_shard += 1
                if current_shard > shard_count:
                    current_shard = 1
                continue
            # else
            if prefix:
                group = prefix + "." + group
            current_shard = DeqpTrie(trie).write_caselist(outfh, group, shard, shard_count, current_shard)
        return current_shard

    def provide_flakes(self, flakes, prefix=""):
        for (test_name, test_result) in self.results():
            full_test_name = test_name
            if prefix:
                full_test_name = f"{prefix}.{test_name}"
            status = test_result.result().lower()
            if status == "notsupported":
                status = "pass"
            if status == "internalerror":
                status = "fail"
            if status == "crash":
                status = "fail"
            if status not in ["pass", "fail"]:
                print("WARN: invalid status: " + test_name + " : " + status)
                status = "fail"
            flakes.add_result(full_test_name, status, test_result.stdout() +  test_result.stderr())
        for group, trie in self._trie.items():
            if group == '__result__':
                continue
            next_prefix = group
            if prefix:
                next_prefix = f"{prefix}.{group}"
            DeqpTrie(trie).provide_flakes(flakes, next_prefix)

    def merge(self, other):
        for (group, subtrie) in iter(other._trie.items()):
            if '__result__' == group:
                continue
            if group in self._trie:
                # subtrie in both tries, we must merge them
                DeqpTrie(self._trie[group]).merge(DeqpTrie(subtrie))
            else:
                # subtrie only in other, so add it to our trie
                self._trie[group] = subtrie

    def add_qpa_blob(self, split_test_name, blob, pid, full_test_name,
                     err=None):
        if err is None:
            err = []
        err = [e for e in err if "ATTENTION: default value of option vblank_mode" not in e]
        err = [e for e in err if "Mesa: " not in e]
        if len(split_test_name) == 1:
            test = split_test_name[0]
            test_result = self.result(test)
            self._trie[test] = {}
            test_result.duration(0.0)
            test_result.stderr("".join(err) + "\npid: {}\n".format(str(pid)))
            try:
                t = et.fromstringlist(blob)
                stat_tag = t.find("./Result")
                interpreted_result = "crash"
                if stat_tag is not None:
                    interpreted_result = stat_tag.attrib["StatusCode"]
                if interpreted_result == "QualityWarning":
                    interpreted_result = "Pass"
                if interpreted_result == "CompatibilityWarning":
                    interpreted_result = "Pass"
                if interpreted_result == "ResourceError":
                    interpreted_result = "crash"
                if interpreted_result in ("Fail", "crash"):
                    out_txt = ""
                    for a_text in t.findall(".//Text"):
                        # text content can be None
                        if a_text.text:
                            out_txt += a_text.text + "\n"
                    for a_result in t.findall(".//Result"):
                        # result content can be None
                        if a_result.text:
                            out_txt += a_result.text + "\n"
                    test_result.stdout(out_txt)
                # get the test duration value
                for number in t.findall("./Number"):
                    if number.attrib["Name"] != "TestDuration":
                        continue
                    duration = float(number.text)
                    if number.attrib["Unit"] == "us":
                        duration /= 1000000.0
                    elif number.attrib["Unit"] == "ms":
                        duration /= 1000.0
                    test_result.duration(duration)
                # save the final result
                test_result.result(interpreted_result)

            except:
                test_result.result("crash")
            return
        # else, we descend the subtrie to find the test result
        group = split_test_name[0]
        if not group in self._trie:
            self._trie[group] = {}
        DeqpTrie(self._trie[group]).add_qpa_blob(split_test_name[1:], blob, pid, full_test_name, err)

    def write_junit(self, of, config, missing_commits):
        of.write("<testsuites>\n")
        for group, subtrie in iter(self._trie.items()):
            if group == '__result__':
                continue
            subtrie = DeqpTrie(subtrie)
            of.write(f' <testsuite name="{group}" tests="{subtrie.results_count()}">\n')
            subtrie._write_junit_tag(of, group, config, missing_commits)
            of.write(" </testsuite>\n")
        of.write("</testsuites>")
        
    def _write_junit_tag(self, of, prefix, config, missing_commits):
        for (test_name, test_result) in self.results():
            status = test_result.result().lower()
            if status == "notsupported":
                status = "skip"
            if status == "internalerror":
                status = "crash"
            if status not in ["pass", "crash", "skip", "fail"]:
                print("WARN: invalid status: " + test_name + " : " + status)
                status = "fail"
            test_result = self.result(test_name)
            config.write_junit(of, prefix, test_name,
                               status,
                               test_result.duration(),
                               test_result.stdout(),
                               test_result.stderr(),
                               missing_commits)
        for group in self._trie:
            if group == '__result__':
                continue
            DeqpTrie(self._trie[group])._write_junit_tag(of, prefix + "." + group, config, missing_commits)

class ConfigFilter(object):
    """parses config files, to filter out test failures that are not regressions"""
    def __init__(self, file_path, options):
        self._expected_fail = {}
        self._expected_crash = {}
        self._fixed = {}
        self._suffix = options.hardware + options.arch
        with open(file_path, "r") as fh:
            p = CaseConfig(allow_no_value=True)
            p.optionxform = str

            p.readfp(fh)
            if p.has_section("expected-failures"):
                for test, commit in p.items("expected-failures"):
                    self._expected_fail[test] = commit
            if p.has_section("expected-crashes"):
                for test, commit in p.items("expected-crashes"):
                    self._expected_crash[test] = commit
                    if test in self._expected_fail:
                        print("ERROR: duplicate test status: " + test)
                        assert test not in self._expected_fail
            if p.has_section("fixed-tests"):
                for test, commit in p.items("fixed-tests"):
                    self._fixed[test] = commit
                    if test in self._expected_fail or test in self._expected_crash:
                        print("ERROR: duplicate test status: " + test)
                    assert test not in self._expected_fail
                    assert test not in self._expected_crash

    def write_junit(self, of,
                    suite, test_name, status, duration, stdout, stderr,
                    missing_commits):
        """
        interprets test status with the config, filtering failures for
        tests which have changed status in the missing_commits.
        """
        full_test_name = suite + "." + test_name
        filtered_status = status
        commit_filter = ""
        if full_test_name in self._expected_fail:
            commit_filter = self._expected_fail[full_test_name]
            if status == "fail":
                filtered_status = "skip"
                stdout += "\nWARN: this test failed as expected."
            if status == "crash":
                stdout += "\nWARN: this test crashed when it was expected to fail."
            if status == "pass":
                filtered_status = "fail"
                stdout += "\nWARN: this test passed when it was expected to fail."
            if status == "skip":
                filtered_status = "fail"
                stdout += "\nWARN: this test skipped when it was expected to fail."
        elif full_test_name in self._expected_crash:
            commit_filter = self._expected_crash[full_test_name]
            if status == "crash":
                filtered_status = "skip"
                stdout += "\nWARN: this test crashed as expected."
            if status == "fail":
                stdout += "\nWARN: this test failed when it was expected to crash."
            if status == "pass":
                filtered_status = "fail"
                stdout += "\nWARN: this test passed when it was expected to crash."
            if status == "skip":
                filtered_status = "fail"
                stdout += "\nWARN: this test skipped when it was expected to crash."
        elif full_test_name in self._fixed:
            commit_filter = self._fixed[full_test_name]

        for word in commit_filter.split():
            if word in missing_commits:
                stdout += "\nWARN: this test had status " + status + \
                          " but changed in " + commit_filter
                filtered_status = "skip"
                break

        if status == "skip" and filtered_status == "skip":
            return
        if status == "pass" and filtered_status == "pass":
            # don't include output from passing tests
            stdout = ""

        # status attrib gets the "real" status of the test.  filtering
        # based on config changes whether the tag has a <failure> or
        # <skipped> subtag, which is how jenkins reports results.  We
        # need the "real" test status for handling bisection.
        of.write("""  <testcase classname="{}" name="{}" status="{}" time="{}">\n""".format(suite,
                                                                                            test_name + "." + self._suffix,
                                                                                            status,
                                                                                            duration))
        if filtered_status == "skip":
            of.write("""   <skipped type="skip"/>\n""")
        if filtered_status == "fail":
            of.write("""   <failure type="fail"/>\n""")
        if filtered_status == "crash":
            of.write("""   <error type="crash"/>\n""")
        if stdout:
            of.write("""   <system-out>{}</system-out>\n""".format(saxutils.escape(stdout)))
        if stderr:
            of.write("""   <system-err>{}</system-err>\n""".format(saxutils.escape(stderr)))
        of.write("""  </testcase>\n""")

class DeqpTester:
    def __init__(self, runtime=None):
        self.o = Options()
        self.pm = ProjectMap()
        self.runtime = runtime if isinstance(runtime, int) else None

    def test(self, binary, list_policy, extra_args=None, env=None,
             cpus=0, log_mem_stats=False):
        if extra_args is None:
            extra_args = []
        if env == None:
            env = {}
        build_root = self.pm.build_root()
        base_env = { "LD_LIBRARY_PATH" : get_libdir(),
                     "LIBGL_DRIVERS_PATH" : get_libgl_drivers(),
                     "INTEL_PRECISE_TRIG" : "1",

                     # without this, Xorg limits frame rate to 1 FPS
                     # when display sleeps, cratering tests execution
                     "vblank_mode": "0"}
        
        for k,v in base_env.items():
            env[k] = v
        self.o.update_env(env)
        all_tests = DeqpTrie()
        if self.o.retest_path:
            testlist = TestLister(self.o.retest_path + "/test/")
            include_tests = testlist.RetestIncludes(self.pm.current_project())
            if not include_tests:
                # we were supposed to retest failures, but there were none
                return all_tests
            with open("retest_caselist.txt", "w") as fh:
                for t in include_tests:
                    fh.write(t)
                    fh.write("\n")
            all_tests.add_txt("retest_caselist.txt")
        else:
            all_tests = list_policy.tests(env)

        list_policy.blacklist(all_tests)
        
        if all_tests.empty():
            return all_tests

        shardno = 0
        shardcount = 0
        if self.o.shard != "0":
            shardargs = self.o.shard.split(":")
            shardno = int(shardargs[0])
            shardcount = int(shardargs[1])

        savedir = os.getcwd()
        os.chdir(os.path.dirname(binary))
        with open("mesa-ci-caselist.txt", "w") as fh:
            all_tests.write_caselist(fh, prefix="", shard=shardno,
                                     shard_count=shardcount)

        # free memory associated with the Trie
        all_tests = None

        shard_tests = DeqpTrie()
        shard_tests.add_txt("mesa-ci-caselist.txt")
        if not cpus:
            cpus = cpu_count(self.o)
        # Disable parallel testing if testing run time of tests
        if self.runtime:
            cpus = 1

        results = self.test_trie(binary, shard_tests,
                                 extra_args=extra_args,
                                 env=env,
                                 cpus=cpus,
                                 log_mem_stats=log_mem_stats)
        os.remove("mesa-ci-caselist.txt")
        # collect any coredumps and move to jenkins tests dir
        coredumps = glob.glob('core.*')
        test_path = self.pm.build_root() + "/../test/"
        if not os.path.exists(test_path):
            os.makedirs(test_path, exist_ok=True)
        for coredump in coredumps:
            shutil.move(os.getcwd() + '/' + coredump, test_path)

        os.chdir(savedir)
        return results

    def test_trie(self, binary, trie, extra_args, env,
                  cpus=0, log_mem_stats=False):
        """execute tests in the trie, returning a results trie"""
        base_commands = [binary,
                         "--deqp-log-images=disable",
                         "--deqp-log-shader-sources=disable",
                         "--deqp-gl-config-name=rgba8888d24s8",
                         "--deqp-surface-width=400",
                         "--deqp-surface-height=300",
                         "--deqp-visibility=hidden"] + extra_args
        savedir = os.getcwd()
        os.chdir(os.path.dirname(binary))
        procs = {}
        out_fh = open(os.devnull, "w")
        procEnv = dict(list(os.environ.items()) + list(env.items()))
        single_proc = False
        if "DEQP_DETECT_GPU_HANG" in env:
            single_proc = True
        tests = {}
        current_test = {}
        full_test_count = trie.test_count()
        print("Total test count: " + str(full_test_count) + "\n")
        for cpu in range(1, cpus + 1):
            case_fn = "mesa-ci-caselist-" + str(cpu) + ".txt"
            out_fn = "TestResults-" + str(cpu) + ".qpa"
            if os.path.exists(out_fn):
                os.remove(out_fn)
            with open(case_fn, "w") as fh:
                trie.write_caselist(fh, prefix="", shard=cpu,
                                    shard_count=cpus)

            # do not execute deqp if no tests have been scheduled for
            # the current cpu core
            core_tests = DeqpTrie()
            core_tests.add_txt(case_fn)
            if core_tests.empty():
                continue
            tests[cpu] =  core_tests

            commands = base_commands + ["--deqp-log-filename=" + out_fn]
            test_name = ""
            if single_proc:
                test_name = core_tests.pop_front()
                current_test[cpu] = test_name
                commands += ["-n", test_name]
            else:
                commands += ["--deqp-caselist-file=" + case_fn,]
            err_fh = tempfile.TemporaryFile("w+")
            proc = subprocess.Popen(commands,
                                    stdout=out_fh,
                                    stderr=err_fh,
                                    env=procEnv)
            if single_proc:
                print(str(proc.pid) + ": " + test_name)

            proc.err_fh = err_fh
            procs[cpu] = proc

        # free memory associated with the Trie
        trie = None
        results = DeqpTrie(runtime=self.runtime)

        completed_tests = 0 # for status only.  accurate count is
        completion_fh = {}  # maintained in the results object.

        # key: cpu. value: time the completion_fh was last read.  Used to
        # determine if a test has deadlocked a thread.
        last_read_time = {}

        completion_interval = 0
        completion_percentage = 0
        internal_hw = ["tgl", "tgl_sim"]
        if internal_vars:
            internal_hw += internal_vars.internal_hardware
        max_crash_cnt = 1000
        if self.o.hardware in internal_hw:
            # For internal platforms, crash count can be higher and that's OK.
            # 100k chosen because there aren't many shards that run with >100k
            # tests.. this might need to be adjusted if that changes..
            max_crash_cnt = 1000000

        crash_cnt = 0
        process_concluded = False

        # invoke tests
        while True:
            if not single_proc:
                if completion_interval % 5 == 0:
                    # print every 5 seconds
                    new_percentage = (completed_tests * 100) / full_test_count
                    if new_percentage > completion_percentage:
                        completion_percentage = new_percentage
                        print("[ " + str(round(completion_percentage, 2))
                              + "% ]")
                    # print memory stats if desired
                    if log_mem_stats:
                        memtotal = None
                        memavail= None
                        with open('/proc/meminfo', 'r') as f:
                            meminfo = f.readlines()
                        for line in meminfo:
                            if line.startswith('MemTotal'):
                                memtotal = line.rstrip()
                            elif line.startswith('MemAvailable'):
                                memavail = line.rstrip()
                            if memtotal and memavail:
                                break
                        print(memavail + " " + memtotal)

                completion_interval += 1
                if not process_concluded:
                    # no tests completed when we polled.  Delay to
                    # avoid a spin loop.
                    time.sleep(1.0)
                else:
                    process_concluded = False
            if not procs:
                break
            for cpu, proc in list(procs.items()):
                out_fn = "TestResults-" + str(cpu) + ".qpa"
                case_fn = "mesa-ci-caselist-" + str(cpu) + ".txt"

                # check completion, to provide some status to the user
                if not single_proc:
                    # single_proc runs without a pause.  Test names
                    # are written to console, so we don't need
                    # percentages.
                    if cpu not in completion_fh:
                        if os.path.exists(out_fn):
                            completion_fh[cpu] = open(out_fn, "r",
                                                      encoding="ISO-8859-1")
                            last_read_time[cpu] = time.time()
                    if cpu in completion_fh:
                        for line in completion_fh[cpu].readlines():
                            # update last read time
                            last_read_time[cpu] = time.time()
                            if line == "#endTestCaseResult\n":
                                completed_tests += 1
                        if time.time() - last_read_time[cpu] > 10 * 60:
                            print(f"Error: deadlock detected on cpu {cpu}.  Terminating test.")
                            proc.terminate()

                proc.poll()
                if proc.returncode is None:
                    continue

                # At this point, the a test process has ended or crashed
                process_concluded = True

                if not single_proc and cpu in completion_fh:
                    completion_fh[cpu].close()
                    del completion_fh[cpu]
                test_count = results.results_count()
                proc.err_fh.seek(0)
                errors = proc.err_fh.readlines()
                proc.err_fh.close()
                self.parse_qpa_results(results, out_fn, pid=proc.pid,
                                       err=errors)
                if test_count == results.results_count():
                    test_name = ""
                    if single_proc:
                        test_name = current_test[cpu]
                    else:
                        # no test executed
                        with open(case_fn, "r") as fh:
                            test_name = fh.readline().strip()
                    results.add_qpa_blob(test_name.split("."),
                                         '<bogus><Result StatusCode="crash"/></bogus>',
                                         proc.pid, test_name, errors)
                unfinished_tests = tests[cpu]
                if not single_proc:
                    unfinished_tests.filter(results)
                if (unfinished_tests.empty()):
                    del procs[cpu]
                    continue
                commands = base_commands + ["--deqp-log-filename=" + out_fn]
                if single_proc:
                    test_name = unfinished_tests.pop_front()
                    commands += ["-n", test_name]
                else:
                    crash_cnt += 1
                    if crash_cnt >= max_crash_cnt:
                        hostname = socket.gethostname()
                        max_crash_text = "FATAL: %i tests have crashed \n" % max_crash_cnt + "aborting remaining tests"

                        fail_and_reboot("max-crash-" + hostname, max_crash_text, self.pm)
                        raise Exception(max_crash_text)
                    with open(case_fn, "w") as fh:
                        unfinished_tests.write_caselist(fh)
                    commands +=  ["--deqp-caselist-file=" + case_fn]
                if os.path.exists(out_fn):
                    os.rename(out_fn, out_fn + "." + str(proc.pid) + "." + datetime.datetime.now().isoformat())
                err_fh = tempfile.TemporaryFile("w+")
                proc = subprocess.Popen(commands,
                                        stdout=out_fh,
                                        stderr=err_fh,
                                        env=procEnv)
                if single_proc:
                    print(str(proc.pid) + ": " + test_name)
                proc.err_fh = err_fh
                procs[cpu] = proc
        os.chdir(savedir)
        return results

    def test_flaky(self, binary, list_policy, env=None):
        if self.o.shard != "0" and not self.o.shard.startswith("1:"):
            return
        if "_sim" in self.o.hardware:
            return
        if self.o.retest_path:
            return

        base_env = { "LD_LIBRARY_PATH" : get_libdir(),
                     "LIBGL_DRIVERS_PATH" : get_libgl_drivers(),
                     "INTEL_PRECISE_TRIG" : "1",

                     # without this, Xorg limits frame rate to 1 FPS
                     # when display sleeps, cratering tests execution
                     "vblank_mode": "0"}
        if env == None:
            env = {}
        for k,v in base_env.items():
            env[k] = v
        self.o.update_env(env)
        flaky_tests = list_policy.flaky(env)

        if flaky_tests.empty():
            return flaky_tests

        for _ in range(5):
            results = self.test_trie(binary, flaky_tests, extra_args=[], env=env,
                                     cpus=cpu_count(self.o))
            results.provide_flakes(list_policy.flakes)
    
    def parse_qpa_results(self, results_trie, filename, pid, err):
        with open(filename, "r", encoding="ISO-8859-1") as qpa:
            current_test = ""
            blob = []
            for line in qpa:
                if line.startswith("#beginTestCaseResult"):
                    line = line.strip()
                    current_test = line[len("#beginTestCaseResult "):]
                    continue
                if line.startswith("#endTestCaseResult"):
                    results_trie.add_qpa_blob(current_test.split("."), blob,
                                              pid, current_test)
                    blob = []
                    current_test = ""
                    continue
                if not current_test:
                    continue
                
                blob.append(line.encode('utf-8'))
            if current_test:
                # crashed
                print("WARN - crashed test: " + current_test)
                results_trie.add_qpa_blob(current_test.split("."), blob, pid, current_test, err)

    def generate_results(self, results_trie, config_policy):
        out_dir = self.pm.build_root() + "/../test"
        if not os.path.exists(out_dir):
            os.makedirs(out_dir)
        deps = DependencyGraph(self.pm.current_project(),
                               Options(args=[sys.argv[0]]),
                               repo_set=None)
        with open(out_dir + "/piglit-" + self.pm.current_project() + "_" + self.o.hardware + "_" + self.o.arch + "_" + self.o.shard + ".xml", "w") as of:
            commits = {}
            for commit in RepoSet().branch_missing_revisions(deps):
                commits[str(commit)] = True
            results_trie.write_junit(of, config_policy, commits)

        check_gpu_hang()

        Export().export_tests()

    def build(self):
        pass
    def clean(self):
        pass

def generation(options):
    if "skl" in options.hardware or "kbl" in options.hardware or "bxt" in options.hardware:
        return 9.0
    if "bdw" in options.hardware or "bsw" in options.hardware:
        return 8.0
    if "hsw" in options.hardware:
        return 7.5
    if "ivb" in options.hardware or "byt" in options.hardware:
        return 7.0
    if "snb" in options.hardware:
        return 6.0
    if "ilk" in options.hardware:
        return 5.0
    assert("g965" in options.hardware or "g33" in options.hardware or "g45" in options.hardware)
    return 4.0


def deqp_external_revisions(project, revisions_dict=None):
    if revisions_dict == None:
        revisions_dict = {}
    src_dir = ProjectMap().project_source_dir(project)
    save_path = sys.path
    sys.path = [os.path.abspath(os.path.normpath(s)) for s in sys.path]
    sys.path = [gooddir for gooddir in sys.path if project not in gooddir]
    sys.path = [src_dir + "/external/"] + sys.path
    external_scripts = ["fetch_sources"]
    if project == "glcts":
        external_scripts.append("fetch_kc_cts")
    for a_script in external_scripts:
        fetch_sources = importlib.import_module(a_script, "repos." + project + "." + a_script)
        for package in fetch_sources.PACKAGES:
            if not isinstance(package, fetch_sources.GitRepo):
                continue
            ext_project = package.baseDir
            if ext_project in external_to_repo:
                ext_project = external_to_repo[ext_project]
            if ext_project in revisions_dict:
                revisions_dict[ext_project].append(package.revision)
            else:
                revisions_dict[ext_project] = [package.revision]
    sys.path= save_path
    return revisions_dict

class DeqpSuiteLister(object):
    """Used to blacklist tests for the dEQP suite.  Should not
       be used for other suites (glescts) based on the dEQP framework."""
    def __init__(self, binary, flakes):
        self.binary = binary
        self.pmap =ProjectMap()
        self.version = None
        self.flakes = flakes
        self._all_tests = None

    def tests(self, env):
        if self._all_tests:
            self._all_tests.seek(0)
            return pickle.load(self._all_tests)

        # don't execute tests that are part of the other suite
        cases_xml = None
        if "gles2" in self.binary:
            cases_xml = "dEQP-GLES2-cases.xml"
        if "gles3" in self.binary:
            cases_xml = "dEQP-GLES3-cases.xml"
        if "gles31" in self.binary:
            cases_xml = "dEQP-GLES31-cases.xml"
        if "egl" in self.binary:
            cases_xml = "dEQP-EGL-cases.xml"
        deqp_dir = os.path.dirname(self.binary)
        os.chdir(deqp_dir)
        cmd = [self.binary,
               "--deqp-runmode=xml-caselist"]
        run_batch_command(cmd, env=env)
        all_tests = DeqpTrie()
        all_tests.add_xml(cases_xml)
        os.chdir(self.pmap.project_build_dir())

        self._all_tests = tempfile.TemporaryFile()
        pickle.dump(all_tests, self._all_tests)
        return all_tests

    def blacklist(self, all_tests):
        """remove denylisted tests from the list that will be executed"""
        blacklist_files = get_blacklists()
        for file in blacklist_files:
            blacklist = DeqpTrie()
            blacklist.add_txt(file)
            all_tests.filter(blacklist)

        if not self.version:
            self.version = mesa_version()

        # filter immediately, since any unsupported tests under these
        # top-level categories will prevent them from being filtered.
        if "gles2" not in self.binary:
            all_tests.filter(["dEQP-GLES2"])
        if "gles31" not in self.binary:
            all_tests.filter(["dEQP-GLES31"])
        if "gles3" not in self.binary:
            all_tests.filter(["dEQP-GLES3"])
        if "egl" not in self.binary:
            all_tests.filter(["dEQP-EGL"])
        all_tests.filter(self.flakes.test_list())

    def flaky(self, env):
        """return a trie of all flaky tests that need to be executed"""
        # use test list to expand flaky wildcards.  This will also
        # filter from the flaky-list tests which do not match the
        # current binary.
        flaky_tests = list(self.flakes.test_list())
        all_tests = self.tests(env)
        for a_test in flaky_tests:
            matches = all_tests.matching_tests(a_test)
            self.flakes.expand_test_group(a_test, matches)

        flaky_trie = DeqpTrie()
        for a_test in self.flakes.test_list():
            flaky_trie.add_line(a_test)

        self.blacklist(flaky_trie)
        return flaky_trie
