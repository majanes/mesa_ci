# Copyright (C) Intel Corp.  2014-2020.  All Rights Reserved.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  **********************************************************************/
#  * Authors:
#  *   Mark Janes <mark.a.janes@intel.com>
#  **********************************************************************/

"""handles synchronization of the build_root with the results directory"""
import glob
import os
import random
import socket
import subprocess
import sys
import tempfile
import time
import xml.sax.saxutils
import xml.etree.cElementTree as et
from utils.command import run_batch_command, rmtree
from utils.utils import reliable_url_open
from options import Options
from project_map import ProjectMap
from project_invoke import ProjectInvoke
from dependency_graph import DependencyGraph
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                "../../..", "mesa_ci_internal"))
try:
    import internal_build_support.vars as internal_vars
except ModuleNotFoundError:
    internal_vars = None

def fail_and_reboot(failure_name, msg, project_map=None):
    """ Create a custom failing test. Reboot if calling this function on a CI tester """
    if not project_map:
        project_map = ProjectMap()
    hostname = socket.gethostname()
    Export(project_map).create_failing_test(failure_name, msg)
    # trigger reboot

    print("ERROR: system must be rebooted.")
    server = project_map.build_spec().master_host()
    url = "http://" + server + "/job/public/job/reboot_single/buildWithParameters?label=" + hostname
    print("opening: " + url)
    reliable_url_open(url, method="POST")
    print("sleeping to allow reboot job to be scheduled.")
    time.sleep(20)

def convert_rsync_path(path, project_map=None):
    if not project_map:
        project_map = ProjectMap()

    hostname = project_map.build_spec().results_hostname()

    repl_path = hostname + ".local::nfs/"
    if path.startswith("/mnt/jenkins/"):
        return path.replace("/mnt/jenkins/", repl_path)
    return path

def abspath2rsync_path(path,
                       fqdn="otc-mesa-ci.local",
                       rsync_share="nfs",
                       replace_str="/mnt/jenkins/"):
    repl_path = f"{fqdn}::{rsync_share}/"

    if path.startswith(replace_str):
        return path.replace(replace_str, repl_path)
    return path

def copy(source, dest, project_map=None):
    source = convert_rsync_path(source, project_map=project_map)
    dest = convert_rsync_path(dest, project_map=project_map)

    run_batch_command(["rsync", "-r", "--mkpath"] +
                      [source, dest],
                      quiet=True)

class Export:
    def __init__(self, project_map=None):
        # todo: provide wildcard mechanism
        self.opt = Options()

        if project_map:
            self._project_map = project_map
        else:
            self._project_map = ProjectMap()

        self.result_path = self.opt.result_path
        if not self.result_path:
            self._dest = None
            return

        self._dest = self.result_path
        self.rsyncd_path = convert_rsync_path(self.result_path, project_map=project_map)
        if self.result_path:
            self._dest = self.rsyncd_path

    def copy(self, source, dest):
        source = convert_rsync_path(source, project_map=self._project_map)
        dest = convert_rsync_path(dest, project_map=self._project_map)
        run_batch_command(["rsync", "-r", "--mkpath"] +
                          [source, dest],
                          quiet=True)

    def delete(self, dest):
        dest = convert_rsync_path(dest, project_map=self._project_map)
        with tempfile.TemporaryDirectory() as tempd:
            run_batch_command(["rsync", "-aP", "--delete",
                               f"{tempd}/",
                               dest],
                              streamedOutput=False,quiet=True)

    def export(self):
        pm = ProjectMap()
        project = pm.current_project()
        if not self.result_path:
            return

        # remove binaries that were already part of the build_root
        rmtree("/tmp/min_build_root")
        cmd = ["rsync", "-rlpDm", "--compare-dest=/tmp/orig_build_root",
               pm.build_root(),
               "/tmp/min_build_root"]
        run_batch_command(cmd)

        cmd = ["rsync", "-rlpD",
               "--mkpath",
               "--exclude='*.o'",
               "--exclude='*.a'",
               "/tmp/min_build_root/" + self.opt.arch,
               f"{self._dest}/{project}/build_root/"]
        try:
            run_batch_command(cmd)
        except subprocess.CalledProcessError as e:
            print("WARN: some errors copying: " + str(e))

        self.export_tests()

        # /tmp in some builders is not large, so clean up
        rmtree("/tmp/min_build_root")

    def export_tests(self):
        if not self.result_path:
            return

        proj_invoke = ProjectInvoke(project_map=self._project_map)
        pm = self._project_map
        test_path = os.path.abspath(f"{pm.build_root()}/../test")
        test_artifacts = []
        os.makedirs(test_path, exist_ok=True)

        # create a copy of the test xml in the source root, where
        # jenkins can access it.  Jenkins does not accept the 'status'
        # attribute in the test_case element.
        jenkins_path = f"{pm.source_root()}/test"
        os.makedirs(jenkins_path, exist_ok=True)

        for a_file in glob.glob(f"{test_path}/*.xml", recursive=True):
            # Remove passing tests from xml and write to different directory so
            # full results are preserved
            a_file_name = a_file.split('/')[-1]
            out_test_file = f"{jenkins_path}/{a_file_name}"
            try:
                xml = et.parse(a_file)
            except et.ParseError:
                continue
            root = xml.getroot()
            if "disabled" in root.attrib:
                del root.attrib["disabled"]
            for a_suite in root.findall("testsuite"):
                if "disabled" in a_suite.attrib:
                    del a_suite.attrib["disabled"]
                for a_test in a_suite.findall("testcase"):
                    if "status" in a_test.attrib:
                        del a_test.attrib["status"]
            xml.write(out_test_file, encoding='utf-8', xml_declaration=True)

        for artifact in [f"{test_path}/*.txt", f"{test_path}/card_error_*"]:
            artifacts = glob.glob(artifact)
            if artifacts:
                cmd = ["cp", "-f"] + artifacts + [jenkins_path]
                run_batch_command(cmd, quiet=True, streamedOutput=False)

        cmd = ["rsync", "-rlpD",
               "--exclude='*.o'",
               "--exclude='*.a'",
               test_path,
               self._dest]

        try:
            run_batch_command(cmd)
        except subprocess.CalledProcessError as e:
            print("WARN: some errors copying: " + str(e))

        test_files = os.listdir(test_path)
        proj_invoke.set_info("test_files", test_files)

        for tf in test_files:
            artifact_type = "log"

            if tf.startswith("flaky_"):
                artifact_type = "flaky"
            elif tf.startswith("disabled_"):
                artifact_type = "disabled"
            elif tf.endswith(".xml"):
                artifact_type = "junit"

            test_artifacts.append({ "file": tf, "type": artifact_type})

        proj_invoke.set_info("artifacts", test_artifacts)


    def export_perf(self):
        if not self.result_path:
            return

        perf_path = ProjectMap().build_root()
        rsync = "rsync"
        dest = self._dest
        if not os.path.exists(perf_path):
            print("ERROR: no results to export")
            return
        cmd = [rsync, "-rlpD", "--exclude=build",
               "--exclude='*.o'",
               "--exclude='*.a'",
               perf_path,
               dest]

        try:
            run_batch_command(cmd)
        except subprocess.CalledProcessError as e:
            print("WARN: some errors copying: " + str(e))

    def result_path_exists(self, path):
        with subprocess.Popen(["rsync", "--list-only",
                               convert_rsync_path(path, project_map=self._project_map)],
                              stdout=subprocess.DEVNULL,
                              stderr=subprocess.DEVNULL) as proc:
            proc.communicate()
            return proc.returncode == 0

    def import_build_root(self, arch=None):
        o = Options()
        arch = arch or o.arch
        result_path = o.result_path
        if not o.result_path:
            return
        if not self.result_path_exists(result_path):
            print("WARN: no build root to import, sleeping")
            time.sleep(10)
        if not self.result_path_exists(result_path):
            print("WARN: no build root to import: " + result_path)
            return

        pm = ProjectMap()
        project = pm.current_project()

        br = os.path.dirname(pm.build_root())
        if not os.path.exists(br):
            os.makedirs(br)

        # import only the prerequisites
        depGraph = DependencyGraph(project, self.opt)
        components = depGraph.all_builds()
        # simulated platforms require simdrm and fulsim
        if '_sim' in self.opt.hardware:
            components += [ProjectInvoke(project='sim-drm'),
                           ProjectInvoke(project='fulsim')]
        for component in components:
            # these components don't have anything in build_root, and just
            # trigger failures in rsync
            skip_components = ['mesa-perf', 'sixonix', 'steam', project]
            if component.project in skip_components:
                continue
            cmd = ["rsync", "-rlpD",
                   "--exclude='*.o'",
                   "--exclude='*.a'",
                   f"{self._dest}/{component.project}/build_root/{arch}",
                   br]
            try:
                run_batch_command(cmd)
            except subprocess.CalledProcessError as e:
                print("WARN: some errors copying: " + str(e))

        hw_needs_br_mesa = ["xe_gen12", "xe_gen125"]
        if internal_vars:
            hw_needs_br_mesa += internal_vars.hw_needs_build_root_mesa
        if self.opt.hardware in hw_needs_br_mesa:
            # use ldconfig to put br libs into ld cache
            cmd = ['ldconfig',
                   br + '/m64/lib',
                   br + '/m32/lib',
                   br + '/m64/lib/dri',
                   br + '/m32/lib/dri',
                   br + '/m64/lib/x86_64-linux-gnu',
                   br + '/m32/lib/i386-linux-gnu']
            try:
                # build systems have ldconfig setuid root
                run_batch_command(cmd)
            except subprocess.CalledProcessError:
                cmd.insert(0, 'sudo')
                run_batch_command(cmd)

        # make a copy of hard links, to be used as a filter for export
        rmtree("/tmp/orig_build_root")
        cmd = ["cp", "-al", br, "/tmp/orig_build_root"]
        run_batch_command(cmd)

        # don't want to confuse test results with any preexisting
        # files in the build root.
        test_dir = os.path.normpath(br + "/../test")
        if os.path.exists(test_dir):
            rmtree(test_dir)

    def create_failing_test(self, failure_name, output):
        o = Options()
        test_path = os.path.abspath(self._project_map.build_root() + "/../test/")
        print("ERROR: creating a failing test: "
              + failure_name + " : " + output)
        if not os.path.exists(test_path):
            os.makedirs(test_path)

        randstr = socket.gethostname() + "_" + str(random.random())[2:6]
        # filname has to begin with piglit for junit pattern match in jenkins
        # to find it.
        fh = open(test_path + "/piglit-fail-" + o.hardware + o.arch + "_" +
                  randstr + ".xml", "w")
        failure_name = failure_name + "." + o.hardware + o.arch
        fh.write("""\
<?xml version="1.0" encoding="UTF-8"?>
<testsuites>
  <testsuite name="generated-failures" tests="1">
    <testcase classname="generated" name="{}" status="fail" time="0">
      <system-out>""".format(failure_name) + xml.sax.saxutils.escape(output) + """</system-out>
      <failure type="fail" />
    </testcase>
  </testsuite>
</testsuites>""")
        fh.close()
        Export(self._project_map).export_tests()

        # create a copy of the test xml in the source root, where
        # jenkins can access it.
        cmd = ["cp", "-a",
               self._project_map.build_root() + "/../test",
               self._project_map.source_root()]
        run_batch_command(cmd)

    def clean_orig_build_root(self):
        rmtree("/tmp/orig_build_root")

    def create_failing_test_xml(self, failure_name, output):
        o = Options()
        failure_name = failure_name + "." + o.hardware + o.arch
        root = et.Element('testsuites')
        suite = et.SubElement(
            root,
            'testsuite',
            name='generated-failures',
            tests='1')
        testcase = et.SubElement(suite, 'testcase', classname='generated',
                                 name=failure_name, status='fail', time='0')
        out = et.SubElement(testcase, 'system-out')
        out.text = xml.sax.saxutils.escape(output)
        failure = et.SubElement(testcase, 'failure', type='fail')
        return root
