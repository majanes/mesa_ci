from __future__ import print_function
import gzip
import os
import sys
import re
import xml.etree.cElementTree as et
from dependency_graph import DependencyGraph
from utils.check_gpu_hang import check_gpu_hang
from utils.command import run_batch_command, rmtree
from export import Export
from utils.utils import (NoConfigFile, get_conf_file, get_libdir,
                         get_libgl_drivers, mesa_version, is_soft_fp64,
                         get_blacklists, Flakes, cpu_count)
from options import Options
from project_map import ProjectMap
from repo_set import RepoSet
from bisect_test import TestLister, PiglitTest


class PiglitLister:
    def __init__(self, suite=None):
        self._suite = suite
        self._tests = {}
        if not suite:
            return
        assert(type(suite) == type([]))
        self._test_dir = ProjectMap().build_root() + "/lib/piglit/tests/"
        assert(os.path.exists(self._test_dir))
        for a_suite in suite:
            meta_file = self._test_dir + a_suite + ".meta.xml"
            if (os.path.exists(meta_file)):
                meta = et.parse(meta_file)
                for profile in meta.findall("Profile"):
                    self.add(PiglitLister([profile.text]))
            else:
                profile_file = self._test_dir + a_suite + ".xml.gz"
                if not os.path.exists(profile_file):
                    # vkrunner profile may not be available on stable
                    # branches.
                    return
                infile = gzip.open(profile_file, 'rb')
                profile = et.parse(infile)
                for test in profile.findall("Test"):
                    name = test.attrib["name"]
                    name = name.replace(".", "_")
                    name = name.replace("@", ".")
                    name = name.replace(":", ".")
                    name = name.replace("=", ".")
                    name = "piglit." + name
                    self._tests[name] = test

    def filter(self, blacklist):
        if str == type(blacklist):
            blacklist = [blacklist]
        if type(blacklist) == type(PiglitLister("")):
            blacklist = blacklist._tests.keys()
        for test in blacklist:
            test = test.replace(":", ".")
            test = test.replace("@", ".")
            test = test.replace("=", ".")
            if not test:
                continue
            if test.startswith("#"):
                continue
            removed_tests = []
            if test in self._tests:
                removed_tests.append(test)
            else:
                # check for tests in a testgruop
                for k in self._tests.keys():
                    if k.startswith(test):
                        removed_tests.append(k)
            for t in removed_tests:
                del self._tests[t]

    def whitelist(self, whitelist):
        for (test, _) in list(self._tests.items()):
            if test not in whitelist:
                del self._tests[test]

    def add(self, testlist):
        self._tests.update(testlist._tests)

    def test_count(self):
        return len(self._tests)

    def write_suite(self, target_suite, shard=None):
        test_count = len(self._tests)
        if shard == "0":
            shard = None
        if shard:
            (shard_num, shards) = shard.split(":")
            shard_num = int(shard_num)
            shards = int(shards)
            shard_count = int(test_count / shards)
            if shard_num <= test_count % shards:
                shard_count += 1
            test_count = shard_count
            shard = 1
        with gzip.open(self._test_dir + '{}.xml.gz'.format(target_suite), 'wb') as f:
            f.write(f"""<?xml version='1.0' encoding='utf-8'?>
<PiglitTestList count="{test_count}" name="{target_suite}">\n""".encode())
            for (_, tag) in sorted(self._tests.items()):
                if not shard or shard == shard_num:
                    f.write(et.tostring(tag) + "\n".encode())
                if shard:
                    shard += 1
                    if shard > shards:
                        shard = 1
            f.write("</PiglitTestList>".encode())
    
class PiglitTester(object):
    def __init__(self, _suite=None, piglit_test=None,
                 env=None, timeout=None, jobs=None, excludes=None):
        if not _suite:
            _suite = ["quick", "vulkan"]
        self._excludes = excludes
        if not self._excludes:
            self._excludes = []
        if type(_suite) != type([]):
            # suite must be a list
            _suite = [_suite]
        self.jobs = jobs
        self._piglit_test = None
        if piglit_test:
            # drop the hw/arch suffix
            self._piglit_test = ".".join(piglit_test.split(".")[:-1])

        self.opt = Options()
        if not self.jobs:
            self.jobs = cpu_count(self.opt)
        self.suite = _suite

        # in bisect or single_test, a test may be in either the cpu or gpu suite.
        # use the quick suite, which is more comprehensive
        if self.opt.retest_path or piglit_test:
            self.suite = ["quick", "vulkan"]

        self.pm = ProjectMap()
        self.build_root = self.pm.build_root()

        self.env = {
            'LD_LIBRARY_PATH': ':'.join([
                get_libdir(),
                os.path.join(get_libdir(), 'dri'),
                os.path.join(self.build_root, 'lib', 'piglit', 'lib'),
            ]),
            "LIBGL_DRIVERS_PATH": get_libgl_drivers(),
             # In the event of a piglit related bug, we want the backtrace
            "PIGLIT_DEBUG": "1",

             # Set the path to include buildroot/bin so fast skipping works
            'PATH': ':'.join([
                os.path.join(self.build_root, 'bin'),
                os.environ['PATH'],
            ]),
        }
        if env:
            self.env.update(env)
        self.timeout = timeout

    def test(self):
        mv = mesa_version(self.env)

        # check the build root to see if hasvk is part of the icd
        # path.  This must happen in the test() routine because the
        # buildroot is not populated until import_build_root is run.
        if self.opt.arch == "m64":
            icd_name = "intel_icd.x86_64.json"
            hasvk_icd_name = "intel_hasvk_icd.x86_64.json"
        elif self.opt.arch == "m32":
            icd_name = "intel_icd.i686.json"
            hasvk_icd_name = "intel_hasvk_icd.i686.json"
        icd_path = f"{self.pm.build_root()}/share/vulkan/icd.d/{icd_name}"
        hasvk_icd_path = f"{self.pm.build_root()}/share/vulkan/icd.d/{hasvk_icd_name}"
        if os.path.exists(hasvk_icd_path):
            icd_path = f"{icd_path}:{hasvk_icd_path}"
        self.env["VK_ICD_FILENAMES"] = icd_path
        self.opt.update_env(self.env)

        # do not run vulkan tests if build is for NIR testing (bug #108957)
        if (('NIR_TEST_CLONE' in self.env or 'NIR_TEST_SERIALIZE' in self.env)
                and 'vulkan' in self.suite):
            self.suite.remove('vulkan')

        out_dir = self.build_root + "/test/" + self.opt.hardware

        hardware = self.opt.hardware
        project = self.pm.current_project()
        try:
            conf_file = get_conf_file(hardware, self.opt.arch, project=project)
        except NoConfigFile:
            print('No config file found for hardware: {0} arch: {1}'.format(hardware, self.opt.arch), file=sys.stderr)
            sys.exit(1)
        
        suffix = self.opt.hardware
        hardware = self.opt.hardware
        if "gt" in hardware:
            hardware = hardware[:3]
        cmd = [self.build_root + "/bin/piglit",
               "run",
               "-o",
               "-p", "gbm",
               "-b", "junit",
               "--junit_suffix", "." + suffix + self.opt.arch,
               "--config", conf_file,
               "--jobs", str(self.jobs),
        ]
        if self.timeout:
            cmd += ['--timeout', str(self.timeout)]
        target_suite = PiglitLister(self.suite)

        # filter out gpu hangs
        for blacklist in get_blacklists():
            target_suite.filter([l.rstrip() for l in open(blacklist)])
        # filter out flaky tests, to be executed separately
        target_suite.filter(Flakes(project, self.opt).test_list())

        if self.opt.retest_path:
            # only test items which previously failed
            test_project = "piglit-test"
            include_tests = TestLister(self.opt.retest_path + "/test/").RetestIncludes(test_project)
            if not include_tests:
                # we were supposed to retest failures, but there were none
                return
            target_suite.whitelist(include_tests)

        if self._piglit_test:
            # support for running a single test
            target_suite.whitelist([self._piglit_test])

        target_suite.write_suite("target_suite", self.opt.shard)

        concurrency_options = ["-c"]
        if "DEQP_DETECT_GPU_HANG" in self.env:
            concurrency_options = ["-1", "-v"]

        streamed_output = True
        if self.opt.retest_path:
            streamed_output = False
        for exclude in self._excludes:
            cmd += ["-x", exclude]
        (_, err) = run_batch_command(cmd + concurrency_options + ["target_suite", out_dir ],
                                       env=self.env,
                                       expected_return_code=[0, 2],
                                       streamedOutput=streamed_output)
        if err and b"No tests were run" in err:
            open(out_dir + "/results.xml", "w").write("<testsuites/>")

        single_out_dir = self.build_root + "/../test"
        if not os.path.exists(single_out_dir):
            os.makedirs(single_out_dir)

        final_file = single_out_dir + "_".join(["/" + self.pm.current_project(),
                                                hardware,
                                                self.opt.arch, self.opt.shard]) + ".xml"
        if os.path.exists(out_dir + "/results.xml"):
            # obtain the set of revisions on master which are not on
            # the current branches.  This set represents the revisions
            # that should cause tests to be disregared.
            deps = DependencyGraph(project,
                                   Options(args=[sys.argv[0]]),
                                   repo_set=None)
            revisions = RepoSet().branch_missing_revisions(deps)
            print("INFO: filtering tests from " + out_dir + "/results.xml")
            # Uniquely name all test files in one directory, for
            # jenkins
            self.filter_tests(revisions,
                              out_dir + "/results.xml",
                              final_file)

        if "bsw" == hardware:
            # run piglit again, to eliminate intermittent failures
            tl = TestLister(final_file)
            retests = tl.RetestIncludes("piglit-test")
            if retests:
                second_out_dir = out_dir + "/retest"
                target_suite.whitelist(retests)
                target_suite.write_suite("retest")
                print("WARN: retesting piglit to " + second_out_dir)
                (_, err) = run_batch_command(cmd +
                                             concurrency_options + ["retest", second_out_dir ],
                                             env=self.env,
                                             expected_return_code=None,
                                             streamedOutput=streamed_output)
                second_results = TestLister(second_out_dir + "/results.xml")
                for a_test in tl.TestsNotIn(second_results):
                    print("stripping flaky test: " + a_test.test_name)
                    a_test.ForcePass(final_file)
                rmtree(second_out_dir)

        self.test_flaky()

        check_gpu_hang()
        Export().export_tests()

    def filter_tests(self, revisions, infile, outfile):
        """this functionality has been duplicated in deqp-test/build.py.  If
        it needs to change, then either change it everywhere or refactor out
        the duplication."""
        t = et.parse(infile)
        for a_suite in t.findall("testsuite"):
            # remove skipped tests, which uses ram on jenkins when
            # displaying and provides no value.
            for a_skip in a_suite.findall("testcase/skipped/.."):
                if a_skip.attrib["status"] in ["crash", "fail"]:
                    continue
                a_suite.remove(a_skip)

            # for each failure, see if there is an entry in the config
            # file with a revision that was missed by a branch
            for afail in a_suite.findall("testcase/failure/..") + a_suite.findall("testcase/error/.."):
                piglit_test = PiglitTest("foo", "foo", afail)
                regression_revision = piglit_test.GetConfRevision()
                abbreviated_revisions = [a_rev[:6] for a_rev in revisions]
                for abbrev_rev in abbreviated_revisions:
                    if abbrev_rev in regression_revision:
                        print("stripping: " + piglit_test.test_name + " " + regression_revision)
                        a_suite.remove(afail)
                        # a test may match more than one revision
                        # encoded in a comment
                        break

            # strip unneeded output from passing tests
            for apass in a_suite.findall("testcase"):
                if apass.attrib["status"] != "pass":
                    continue
                if apass.find("failure") is not None:
                    continue
                out_tag = apass.find("system-out")
                if out_tag is not None:
                    apass.remove(out_tag)
                err_tag = apass.find("system-err")
                if err_tag is not None and err_tag.text is not None:
                    found = False
                    for a_line in err_tag.text.splitlines():
                        m = re.match("\s*pid: \[([0-9]+)\]\s*", a_line)
                        if m is not None:
                            found = True
                            err_tag.text = a_line
                            break
                    if not found:
                        apass.remove(err_tag)

        t.write(outfile)

    def test_flaky(self):
        if self.opt.retest_path or self._piglit_test:
            # we don't re-test/bisect/accept flaky tests
            return
        if not self.opt.shard.startswith("1:") and self.opt.shard != "0":
            # only test flakes in the first shard
            return
        if "_sim" in self.opt.hardware:
            return

        self.opt.update_env(self.env)

        out_dir = self.build_root + "/test/" + self.opt.hardware

        project = self.pm.current_project()
        flakes = Flakes(project, self.opt)
        if not flakes.test_list():
            return

        cmd = [self.build_root + "/bin/piglit",
               "run",
               "-o",
               "-p", "gbm",
               "-b", "junit",
               "-c",
               ]
        target_suite = PiglitLister(self.suite)
        target_suite.whitelist(flakes.test_list())

        # filter out anything in a deny list
        for denylist in get_blacklists():
            for deny in open(denylist):
                deny = deny.rstrip()
                target_suite.filter(deny)
                flakes.deny_test(deny)
        if target_suite.test_count() == 0:
            return

        target_suite.write_suite("flaky_suite")

        for i in range(10):
            print(f"\nExecuting flaky iteration {str(i+1)} out of 10")
            run_batch_command(cmd + ["flaky_suite", out_dir ],
                              env=self.env,
                              expected_return_code=[0, 2],
                              streamedOutput=True)
            # parse result file into flakes
            result_xml_path = f"{out_dir}/results.xml"
            assert os.path.exists(result_xml_path)
            results = et.parse(result_xml_path)
            for test in results.findall(".//testcase"):
                test_name = f"{test.attrib['classname']}.{test.attrib['name']}"
                status = test.attrib["status"]
                if status == "crash":
                    status = "fail"
                if status == "skip":
                    status = "pass"
                assert status in ["pass", "fail"]
                output = ""
                if status == "fail":
                    output = test.find("system-out").text
                # always add stderr, which has the pid
                output += test.find("system-err").text
                flakes.add_result(test_name, status, output)
            rmtree(out_dir)
        flakes.generate_xml()

    def build(self):
        """This project is not compiled"""

    def clean(self):
        """This project is not cleaned"""
