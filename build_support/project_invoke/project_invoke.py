# Copyright (C) Intel Corp.  2018.  All Rights Reserved.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  **********************************************************************/
#  * Authors:
#  *   Mark Janes <mark.a.janes@intel.com>
#  **********************************************************************/
import hashlib
import json
import os
import random
import re
import subprocess
import sys
import tempfile
import time
import xml.etree.cElementTree as et

from options import Options
from project_map import ProjectMap
from repo_set import RepoSet, RevisionSpecification

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                             "../../..", "mesa_ci_internal"))
try:
    import internal_build_support.vars as internal_vars
except ModuleNotFoundError:
    internal_vars = None

class DotQuad:
    def __init__(self, from_str):
        quad_str = from_str.split('.')
        self.quad = []
        for q in quad_str:
            self.quad.append(int(re.match('\d+', q)[0]))

    @classmethod
    def from_file(cls, version_file):
        with open(version_file, "r") as fh:
            ver_str = fh.read()
            return DotQuad(ver_str)

    def __lt__(self, a):
        return self.quad < a.quad

    def __eq__(self, a):
        return self.quad == a.quad

def convert_rsync_path(path, project_map=None):
    hostname = "otc-mesa-ci"
    repl_path = hostname + ".local::nfs/"
    if path.startswith("/mnt/jenkins/"):
        return path.replace("/mnt/jenkins/", repl_path)
    return path

class ProjectInvoke:
    """this object summarizes the component and all options required to
    invoke a build on a single project.  Invocation can take place
    locally or on CI.  ProjectInvoke supports writing status files for
    the invoked build to a network folder, to prevent duplicate builds.

    """

    def __init__(self, options=None, revision_spec=None,
                 project=None, from_string=None, repo_set=None, project_map=None):
        if from_string:
            self.from_string(from_string)
            return

        if not options:
            options = Options()
        self.options = options

        if not project:
            if not project_map:
                project_map = ProjectMap()
            project = project_map.current_project()
        self.project = project

        if not revision_spec:
            if not project_map:
                project_map = ProjectMap()
            revision_spec = RevisionSpecification(repo_set=repo_set, project_map=project_map)
        self.revision_spec = revision_spec

    def __str__(self):
        tag = et.Element("ProjectInvoke")
        tag.set("Project", self.project)
        tag.append(et.fromstring(str(self.revision_spec)))
        tag.append(self.options.to_elementtree())
        return et.tostring(tag, encoding="UTF-8").decode()

    def from_string(self, string):
        tag = et.fromstring(string)
        self.project = tag.attrib["Project"]
        self.options = Options(from_xml=tag.find("Options"))
        revtag = tag.find("RevSpec")
        self.revision_spec = RevisionSpecification(revisions=revtag.attrib)

    def run_cmd(self, cmd):
        """wrap  subprocess for common rsync commands"""
        with subprocess.Popen(cmd,
                              stdout=subprocess.DEVNULL,
                              stderr=subprocess.DEVNULL) as proc:
            proc.communicate()
            return proc.returncode

    def file_exists(self, path):
        return 0 == self.run_cmd(["rsync", "--list-only", convert_rsync_path(path)])

    def info_file(self, relative_path=False):
        o = self.options
        shard_str = ""
        if o.shard != 0:
            shard_str = "_" + o.shard

        prefix = [o.result_path] if not relative_path else ['.']
        return "/".join(prefix +
                        [self.project,
                         o.arch,
                         o.config,
                         o.hardware,
                         "_build_info" + shard_str + ".txt"])

    def _read_info(self):
        """returns a dictionary of status content"""
        info_file = self.info_file()
        if not self.file_exists(info_file):
            # sometimes network/mount hiccups make it seem like the
            # file is not there
            time.sleep(0.2)
            if not self.file_exists(info_file):
                return {}

        attempt_number = 0
        while attempt_number < 5:
            attempt_number += 1
            with tempfile.TemporaryDirectory() as tmpdir:
                if 0 == self.run_cmd(["rsync",
                                      convert_rsync_path(info_file),
                                      f"{tmpdir}/info.json"]):

                    with open(f"{tmpdir}/info.json", "r", encoding="utf8") as infofh:
                        info_text = infofh.read()
                        info_dict = json.loads(info_text)
                        return info_dict
                # network hiccup
                print("WARN: network hiccup detected")
                time.sleep(5)

        # failed to parse several times.
        return {}

    def _write_info(self, info_dict):
        info_file = self.info_file()
        with tempfile.NamedTemporaryFile() as tempf:
            tempf.write(json.dumps(info_dict).encode())
            tempf.flush()
            self.run_cmd(["rsync", "--mkpath", tempf.name, convert_rsync_path(info_file)])
            tempf.close()

    def get_info(self, key, block=True):
        for _ in range(0, 10):
            info = self._read_info()
            if key in info:
                return info[key]
            if not block:
                return None
            # possible that the data has not been flushed to the
            # server
            time.sleep(1)

    def set_info(self, key, value):
        random.seed()
        attempt_count = 0
        while attempt_count < 10:
            info_dict = self._read_info()
            info_dict[key] = value
            self._write_info(info_dict)
            if value == self.get_info(key, block=False):
                # verify that another read/write did not occur around our
                # write/check.  Writes should take less than 1s.  A second
                # read will verify that the value was not changed after
                # our check.
                time.sleep(1)
                if value == self.get_info(key, block=False):
                    return
            attempt_count += 1
            print(f"WARN: retrying set build info: {key}, {value}")
            time.sleep(random.random() * 3)
        print(f"WARN: failed to set build info: {key}, {value}")

    def hash(self, salt):
        """provides a string value to uniquely identify a build.  This is used
        to find builds and resolve clashes between similar builds on
        the jenkins server"""
        return hashlib.md5((salt + str(self)).encode("utf-8")).hexdigest()

    def to_short_string(self):
        items = [self.project,
                 self.options.arch,
                 self.options.config,
                 self.options.hardware]
        if self.options.shard != "0":
            items.append(self.options.shard)
        return " ".join(items)

    # Create methods to parse version from source for other projects here
    def get_mesa_version(self):
        mesa_version_file = ProjectMap().project_source_dir("mesa") + "/VERSION"
        return DotQuad.from_file(mesa_version_file)

    # add entries for other projects which support hardware only
    # beyond certain versions.
    version_methods = { "mesa" : get_mesa_version }

    def projects_support_hardware(self):
        if not internal_vars:
            return True
        for project in self.revision_spec.projects():
            if project not in ProjectInvoke.version_methods:
                continue
            if project not in internal_vars.minimum_version:
                continue
            hw_req = internal_vars.minimum_version[project]
            hw = self.options.hardware
            if hw not in hw_req:
                continue
            version = ProjectInvoke.version_methods[project](self)
            required_version = DotQuad(internal_vars.minimum_version[project][hw])
            if version < required_version:
                return False
        return True
