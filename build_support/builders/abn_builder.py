# Copyright (C) Intel Corp.  2020.  All Rights Reserved.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  **********************************************************************/
#  * Authors:
#  *   Clayton Craft <clayton.a.craft@intel.com>
#  **********************************************************************/
import os
import subprocess
import sys
import glob
import urllib
import tempfile
try:
    from bs4 import BeautifulSoup
except:
    pass
from utils.command import rmtree, run_batch_command
from export import Export
from options import Options
from project_map import ProjectMap
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                             "../../..", "mesa_ci_internal"))
try:
    import internal_build_support.vars as internal_vars
except ModuleNotFoundError:
    internal_vars = None

# Note: values are urls templates, {0} will be replaced with the abn build
# number
abn_urls = []
abn_ci_build_url = None
if internal_vars:
    abn_urls += internal_vars.abn_urls
    abn_ci_build_url = internal_vars.abn_ci_build_url

ABN_CACHE_CAPACITY = 100
ABN_CACHE_EVICT_AMOUNT = 1
ABN_CACHE_PATH = "/mnt/jenkins/abn_cache"
ABN_CACHE_MAX_RETRIES = 3

# TODO: These two classes share a lot of code with fulsim_builder, and should be
# made reusable at some point


class AbnBuilder():
    def __init__(self, buildnum):
        self._options = Options()
        self._project_map = ProjectMap()
        self._env = {}
        self.buildnum = buildnum
        # Install path is /tmp/build_root/opt/abn
        self.abn_install_path = (self._project_map.build_root()
                                 + '/opt/abn')

    def build(self):
        if self.buildnum.lower() == "latest":
            # any exception from urlopen is allowed to end the program, since
            # we don't want to progress further if this fails and there's no
            # clean up to do from handling it
            with urllib.request.urlopen(abn_ci_build_url) as resp:
                if resp.status != 200:
                    assert False, ("ERROR: unable to get latest ABN build: "
                                   f"Status: {resp.status}, "
                                   f"headers: {resp.headers.as_string()}")
                soup = BeautifulSoup(resp.read(), 'html.parser')
            buildnums = sorted([a.text for a in soup.find_all('a')],
                               reverse=True)
            latest_buildnum = buildnums[0]
            if latest_buildnum.startswith('ABN-v'):
                self.buildnum = latest_buildnum[5:].rstrip('/')
            else:
                assert False, ("ERROR: unable to get latest ABN build, "
                               "unsupported version format:"
                               f" {latest_buildnum}")

        abn_archive_path = os.path.join("/tmp", "abn_" + self.buildnum +
                                        ".zip")
        AbnCache(self.buildnum).get(abn_archive_path)

        if not os.path.exists(self.abn_install_path):
            os.makedirs(self.abn_install_path)

        print("Extracting abn archive {}".format(abn_archive_path))

        try:
            run_batch_command(["unzip", "-q", abn_archive_path, "-d",
                               self.abn_install_path])
        except subprocess.CalledProcessError:
            if os.path.exists(self.abn_install_path):
                rmtree(self.abn_install_path)
            assert False, ("ERROR: The abn archive is invalid and as been "
                           "removed. This could be caused by a corrupted "
                           "download or if the requested abn version is "
                           "no longer available.")
        finally:
            os.remove(abn_archive_path)

        # apply any patches
        save_dir = os.getcwd()
        patches = glob.glob(self._project_map.project_build_dir() + '/*.patch')
        abn_dir = glob.glob(self.abn_install_path + '/ABN-v*')[0]
        os.chdir(abn_dir)
        for patch in patches:
            try:
                run_batch_command(['patch', '-p1', '-f', '-i', patch])
            except subprocess.CalledProcessError:
                print("WARN: Unable to apply patch to ABN: " + patch)
                continue
        os.chdir(save_dir)
        Export().export()

    def clean(self):
        if os.path.exists(self.abn_install_path):
            rmtree(self.abn_install_path)

    def test(self):
        pass


class AbnCache():
    def __init__(self, buildnum):
        self._buildnum = buildnum
        self._key = self._cache_key()

    def get(self, download_path="/tmp", retries=ABN_CACHE_MAX_RETRIES):
        if retries < 0:
            raise FileNotFoundError("FATAL ERROR: abn archive couldn't be "
                                    "downloaded")

        if retries > ABN_CACHE_MAX_RETRIES:
            retries = ABN_CACHE_MAX_RETRIES

        try:
            if not os.path.exists(ABN_CACHE_PATH):
                self._get_remote_archive(download_path)
                return

            if not os.path.exists(self._key):
                self._cache_miss()

            run_batch_command(["touch", "-c", self._key])
            run_batch_command(["rsync", self._key, download_path])
        except (subprocess.CalledProcessError, FileNotFoundError):
            print("WARN: Failed to copy abn archive. Retrying...")
            self.get(download_path, retries - 1)

        print("abn archive downloaded from cache successfully")

    def _cache_miss(self):
        # SOMEWHAT thread safe; unlikely, but this method might delete more
        # archives than expected
        print("WARN: abn cache miss")
        cache_len = len(os.listdir(ABN_CACHE_PATH))

        if cache_len >= ABN_CACHE_CAPACITY:
            print("WARN: abn cache full, removing old archives")
            rm_files = glob.glob(os.path.join(ABN_CACHE_PATH, "*"))

            rm_files.sort(key=os.path.getmtime)
            for f in rm_files[0:ABN_CACHE_EVICT_AMOUNT]:
                try:
                    tmp = ""
                    if f.startswith(os.path.join(ABN_CACHE_PATH, "abn")) and f.endswith(".zip"):
                        (_, tmp) = tempfile.mkstemp(dir=ABN_CACHE_PATH)
                        # atomic per POSIX standard
                        os.rename(f, tmp)
                        os.remove(tmp)
                except (FileNotFoundError, IsADirectoryError):
                    pass
                finally:
                    if os.path.isfile(tmp):
                        os.remove(tmp)

        print("INFO: attempting to add archive to abn cache")
        self._get_remote_archive(self._key)
        print("INFO: " + os.path.basename(self._key) + " added to abn cache")

    def _get_remote_archive(self, download_path):
        if os.path.isdir(download_path):
            file_path = os.path.join(download_path,
                                     os.path.basename(self._key))
        else:
            file_path = download_path
        # may attempt to speed up later
        for url in abn_urls:
            try:
                tmp = ""
                print("Attempting to download abn from: "
                      "{}".format(url.format(self._buildnum)))
                (_, tmp) = tempfile.mkstemp(dir=os.path.dirname(file_path))
                urllib.request.urlretrieve(url.format(self._buildnum), tmp)
                # atomic per POSIX standard
                os.rename(tmp, file_path)
                break
            except urllib.error.HTTPError:
                print("The requested abn version was not found at the given "
                      "url.")
            finally:
                if os.path.isfile(tmp):
                    os.remove(tmp)

        if os.path.exists(file_path):
            print("abn archive copied to " + file_path)
        else:
            raise FileNotFoundError("ERROR: Unable to retrieve the requested "
                                    "abn version")

        return file_path

    def _cache_key(self):
        return os.path.join(ABN_CACHE_PATH, "abn" + "_" + self._buildnum +
                            ".zip")
