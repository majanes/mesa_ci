# Copyright (C) Intel Corp.  2014.  All Rights Reserved.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  **********************************************************************/
#  * Authors:
#  *   Mark Janes <mark.a.janes@intel.com>
#  **********************************************************************/
import glob
import os
import re
import shutil
import subprocess
try:
    from urllib2 import urlopen
except:
    from urllib.request import urlopen
import xml.etree.cElementTree as et
from utils.command import run_batch_command, rmtree
from export import Export
from utils.utils import (get_package_config_path, get_libdir,
                         get_libgl_drivers, cpu_count, git_clean)
from options import Options
from project_map import ProjectMap
from gtest import GTest


def delete_src_pyc(path):
    for dirpath, _, filenames in os.walk(path):
        for each_file in filenames:
            if each_file.endswith('.pyc'):
                if os.path.exists(os.path.join(dirpath, each_file)):
                    os.remove(os.path.join(dirpath, each_file))


class AutoBuilder(object):

    def __init__(self, o=None, configure_options=None, export=True,
                 opt_flags="", install=True):
        self._options = o
        self._tests = None
        self._export = export
        self._opt_flags = opt_flags
        self.install = install

        self._configure_options = configure_options
        if not configure_options:
            self._configure_options = []

        if not o:
            self._options = Options()

        self._project_map = ProjectMap()
        project = self._project_map.current_project()
        self._project = project

        self._src_dir = self._project_map.project_source_dir(project)
        self._build_root = self._project_map.build_root()
        self._build_dir = self._src_dir + "/build_" + self._options.arch

        self._env = {}
        self._options.update_env(self._env)

    def build(self):
        if not os.path.exists(self._build_root):
            os.makedirs(self._build_root)
        if not os.path.exists(self._build_dir):
            os.makedirs(self._build_dir)

        optflags = self._opt_flags
        if self._options.config != "debug":
            optflags = "-O2 -DNDEBUG"
            
        savedir = os.getcwd()
        pkg_config = get_package_config_path()
        os.chdir(self._build_dir)
        flags = []
        if self._options.arch == "m32":
            flags = ["CFLAGS=-m32 -msse -msse2 " + optflags,
                     "CXXFLAGS=-m32 -msse -msse2 " + optflags, 
                     "--enable-32-bit",
                     "--host=i686-pc-linux-gnu"]
        else:
            flags = ["CFLAGS=-m64 " + optflags,
                     "CXXFLAGS=-m64 " + optflags]

        os.chdir(self._src_dir)
        run_batch_command(["autoreconf", "--verbose", "--install", "-s"], env=self._env)
        os.chdir(self._build_dir)

        env = self._env.copy()
        env.update({
            "PKG_CONFIG_PATH": pkg_config,
            "LD_LIBRARY_PATH": get_libdir(),
            "CC": "ccache gcc -" + self._options.arch,
            "CXX": "ccache g++ -" + self._options.arch,
        })

        run_batch_command([self._src_dir + "/configure",
                           "--prefix=" + self._build_root] + \
                          flags + self._configure_options, env=env)

        run_batch_command(["make",  "-j",
                           str(cpu_count(self._options))], env=self._env)
        if self.install:
            run_batch_command(["make",  '-j', str(cpu_count(self._options)), "install"],
                              env=self._env)

        os.chdir(savedir)

        if self._export:
            Export().export()

    def SetGtests(self, tests):
        self._tests = GTest(binary_dir = self._build_dir, executables=tests)

    def test(self):
        savedir = os.getcwd()
        os.chdir(self._build_dir)

        try:
            run_batch_command(["make",  "-k", "-j",
                               str(cpu_count(self._options)),
                               "check"], env=self._env)
        except(subprocess.CalledProcessError):
            print("WARN: make check failed")
            os.chdir(savedir)
            # bug 91773
            # Export().create_failing_test(self._project +
            #                              "-make-check-failure", "")
        os.chdir(savedir)

        if self._tests:
            self._tests.run_tests()

        if self._export:
            Export().export()

    def clean(self):
        git_clean(self._src_dir)
        if self._build_dir != self._src_dir:
            rmtree(self._build_dir)
            assert(not os.path.exists(self._build_dir))

class CMakeBuilder(object):
    def __init__(self, extra_definitions=None, compiler="gcc", install=True):
        self._options = Options()
        self._project_map = ProjectMap()
        self._compiler = compiler
        self._install = install

        if not extra_definitions:
            extra_definitions = []
        self._extra_definitions = extra_definitions

        project = self._project_map.current_project()

        self._src_dir = self._project_map.project_source_dir(project)
        self._build_root = self._project_map.build_root()
        self._build_dir = self._src_dir + "/build_" + self._options.arch

    def build(self):

        if not os.path.exists(self._build_dir):
            os.makedirs(self._build_dir)

        pkg_config = get_package_config_path()
        savedir = os.getcwd()
        os.chdir(self._build_dir)
        env = {
            'PKG_CONFIG_PATH': get_package_config_path(),
            'CC': 'ccache {}'.format('gcc' if self._compiler != 'clang' else 'clang'),
            'CXX': 'ccache {}'.format('g++' if self._compiler != 'clang' else 'clang++'),
            'CFLAGS': '-m64' if self._options.arch == 'm64' else '-m32',
            'CXXFLAGS': '-m64' if self._options.arch == 'm64' else '-m32',
            # Meson does not set an rpath by default, that is project
            # decicision (CMake does the same, but Autotools does set rpath by
            # default). To work around this for projects (like mesa) that
            # don't, we set the LD_LIBRARY_PATH, This avoids problems like
            # libEGL trying to open an older system libgm.
           'LD_LIBRARY_PATH': get_libdir(),
        }
        self._options.update_env(env)
        run_batch_command(["cmake", "-GNinja", self._src_dir,
                           "-DCMAKE_INSTALL_PREFIX:PATH=" + self._build_root] \
                          + self._extra_definitions, env=env)

        run_batch_command(["ninja", "-j" + str(cpu_count(self._options))], env=env)
        if self._install:
            print("Installing: output suppressed")
            run_batch_command(["ninja", "install"], streamedOutput=False, quiet=True)

        os.chdir(savedir)

        Export().export()

    def clean(self):
        git_clean(self._src_dir)
        rmtree(self._build_dir)
        assert(not os.path.exists(self._build_dir))
        
    def test(self):
        savedir = os.getcwd()
        os.chdir(self._build_dir)

        env = {}
        self._options.update_env(env)

        # get test names
        command = ["ctest", "-V", "-N"]
        (out, _) = run_batch_command(command, streamedOutput=False, quiet=True, env=env)

        os.chdir(savedir)

        out = out.splitlines()
        for aline in out:
            # execute each command reported by ctest
            match = re.match(".*Test command: (.*)", aline.decode())
            if not match:
                continue
            (bin_dir, exe) = os.path.split(match.group(1))
            #bs.GTest(bin_dir, exe, working_dir=bin_dir).run_tests()

class MesonBuilder(object):

    def __init__(self, extra_definitions=None, compiler="gcc", install=True,
                 cpp_args=None, build_dir=None, install_dir=None,
                 project=None):
        self._options = Options()
        self._project_map = ProjectMap()
        self._compiler = compiler
        self._install = install
        self._extra_definitions = extra_definitions or []
        self._cpp_args = cpp_args

        if not project:
            project = self._project_map.current_project()
        self._project = project

        self._src_dir = self._project_map.project_source_dir(project)
        self._build_root = (self._project_map.build_root() if not install_dir
                            else install_dir)

        self._build_dir = build_dir
        if not build_dir:
            self._build_dir = os.path.join(
                self._src_dir, '_'.join(['build', project, self._options.arch]))

    def build(self):
        if not os.path.exists(os.path.join(self._src_dir, "meson.build")):
            return
        pm = ProjectMap()
        env = {
            'PKG_CONFIG_PATH': get_package_config_path(),
            'CC': 'ccache {}'.format('gcc' if self._compiler != 'clang' else 'clang'),
            'CXX': 'ccache {}'.format('g++' if self._compiler != 'clang' else 'clang++'),
            # Meson does not set an rpath by default, that is project
            # decicision (CMake does the same, but Autotools does set rpath by
            # default). To work around this for projects (like mesa) that
            # don't, we set the LD_LIBRARY_PATH, This avoids problems like
            # libEGL trying to open an older system libgm.
            'LD_LIBRARY_PATH': get_libdir(),
            'PATH' : f"{self._build_root}/bin:{os.environ['PATH']}"
        }
        print(f"path: {env['PATH']}")
        if self._cpp_args:
            env["CPPFLAGS"] = self._cpp_args

        self._options.update_env(env)

        returnto = os.getcwd()
        os.chdir(self._src_dir)

        # apply patches if they exist
        git_env = {"GIT_AUTHOR_NAME" : "Mesa CI",
                   "GIT_AUTHOR_EMAIL" : "mesa_ci@intel.com",
                   "GIT_COMMITTER_NAME" : "Mesa CI",
                   "GIT_COMMITTER_EMAIL" : "mesa_ci@intel.com",
                   "EMAIL" : "mesa_ci@intel.com" }
        for patch in sorted(glob.glob(pm.project_build_dir() + "/*.patch")):
            try:
                run_batch_command(["git", "am", patch], env = git_env)
            except:
                print("WARN: failed to apply patch: " + patch)
                run_batch_command(["git", "am", "--abort"], env = git_env)

        # Set libdir to make deqp happy on debian, since otherwise the GLES
        # header dedection will fail.
        if self._options.arch == 'm32':
            cross_file = ['--cross-file', os.path.join(
                os.path.dirname(__file__),
                'x86-linux-gcc.cross' if self._compiler != 'clang' else 'x86-linux-clang.cross')]
        else:
            cross_file = []
        cmd = ['meson', self._build_dir, '--wrap-mode', 'nodownload',
               '--prefix', self._build_root,
               '--libdir', 'lib'] + cross_file + self._extra_definitions
        if os.path.exists(self._build_dir):
            cmd.append('--reconfigure')
        run_batch_command(cmd,
                           env=env)
        # Print out the options builddir is configured with
        run_batch_command(['meson', 'configure', self._build_dir])
        run_batch_command(['ninja', '-j', str(cpu_count(self._options)), '-C',
                           self._build_dir], env=env)
        if self._install:
            print("Installing: output suppressed")
            run_batch_command(['ninja', '-C', self._build_dir, 'install'],
                              streamedOutput=False, quiet=True)

        os.chdir(returnto)

        Export().export()

    def test(self):
        """Run meson tests and package for CI display"""
        exporter = Export()
        config = self._options.config
        arch = self._options.arch
        hdw = self._options.hardware
        project = self._project

        if not os.path.exists(os.path.join(self._src_dir, "meson.build")):
            return
        results_dir = os.path.abspath(os.path.join(
            self._project_map.build_root(), "../test"))
        if not os.path.exists(results_dir):
            os.makedirs(results_dir)

        # possible bug if flag -v present meson will not generate .xml
        proc = subprocess.run(['meson', 'test', '-C', self._build_dir], check=True)
        if proc.returncode != 0:
            exporter.create_failing_test(f'meson test {config} {arch} {hdw}',
                                         'WARN: failed to run meson test')
            return

        # FIXME: We should use a common prefix in the build specifications
        # so we don't need to use piglit here.
        result_path = f"{results_dir}/piglit-mesontest_{project}_{config}_{arch}_{hdw}.xml"
        meson_test_xml = f"{self._build_dir}/meson-logs/testlog.junit.xml"
        if not os.path.exists(meson_test_xml):
            exporter.create_failing_test(
                f'meson test {self._options.config} {self._options.arch} {self._options.hardware}',
                f'WARN: Could not find meson test xml file: {meson_test_xml}')
            exporter.export_tests()
            return

        xml = et.parse(meson_test_xml)

        # mesa unit tests have bad characters in their names, which
        # make it impossible to aggregate them into groups.
        repl_strings = [(" / ", "."),
                        (" ", "_"),
                        (":", "."),
                        ("/", ".")]

        for a_suite in xml.findall("testsuite"):
            for (bad, good) in repl_strings:
                a_suite.attrib["name"] = a_suite.attrib["name"].replace(bad, good)
            for a_test in a_suite.findall("testcase"):
                for (bad, good) in repl_strings:
                    a_test.attrib["name"] = a_test.attrib["name"].replace(bad, good)
                # CI requires the 'platform' that the test was
                # executed on to be the final component of the test
                # name.  Append 'meson' as the platform for every
                # test.
                a_test.attrib["name"] += f".meson{arch}"
                # mesa buildtests set 'classname', which is poorly
                # organized and clutters the test groupings.  Always
                # use the suite name instead.
                a_test.attrib["classname"] = a_suite.attrib["name"]
        xml.write(result_path)
        exporter.export_tests()

    def clean(self):
        git_clean(self._src_dir)
        rmtree(self._build_dir)
        assert not os.path.exists(self._build_dir)
