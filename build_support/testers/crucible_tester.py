import os
import os.path as path
import xml.etree.ElementTree as ET
import configparser
from options import Options
from project_map import ProjectMap
from repo_set import RepoSet
from utils.command import run_batch_command
from export.export import Export
from utils.utils import (get_conf_file, NoConfigFile, mesa_version,
                         get_blacklists, Flakes)
from utils.check_gpu_hang import check_gpu_hang
from bisect_test import TestLister


def post_process_results(xml):
    t = ET.parse(xml)
    o = Options()
    conf = None
    long_revisions = RepoSet().branch_missing_revisions()
    missing_revisions = [a_rev[:6] for a_rev in long_revisions]
    try:
        conf = get_conf_file(o.hardware, o.arch,
                             project=ProjectMap().current_project())
    except NoConfigFile:
        pass
    if conf:
        # key=name, value=status
        expected_status = {}
        changed_commit = {}
        c = configparser.SafeConfigParser(allow_no_value=True)
        c.read(conf)
        for section in c.sections():
            for (test, commit) in c.items(section):
                if test in expected_status:
                    raise Exception("test has multiple entries: " + test)
                expected_status[test] = section
                changed_commit[test] = commit
        testsuite = t.find('./testsuite')
        for atest in testsuite.findall(".//testcase"):
            test_name = atest.attrib["name"].lower()
            if atest.attrib["status"] == "lost":
                atest.attrib["status"] = "crash"
            if atest.attrib["status"] == "skip":
                testsuite.remove(atest)
                continue
            if test_name not in expected_status:
                continue

            expected = expected_status[test_name]
            test_is_stale = False
            for missing_commit in missing_revisions:
                if missing_commit in changed_commit[test_name]:
                    test_is_stale = True
                    # change stale test status to skip
                    for ftag in atest.findall("failure"):
                        atest.remove(ftag)
                    for ftag in atest.findall("error"):
                        atest.remove(ftag)
                    atest.append(ET.Element("skipped"))
                    so = ET.Element("system-out")
                    so.text = ("WARN: the results of this were changed by "
                               + changed_commit[test_name])
                    so.text += ", which is missing from this build."
                    atest.append(so)
                    break
            if test_is_stale:
                continue

            if expected == "expected-failures":
                # change fail to pass
                if atest.attrib["status"] == "fail":
                    for ftag in atest.findall("failure"):
                        atest.remove(ftag)
                    so = ET.Element("system-out")
                    so.text = "Passing test as an expected failure"
                    atest.append(so)
                elif atest.attrib["status"] == "crash":
                    atest.append(ET.Element("failure"))
                    so = ET.Element("system-out")
                    so.text = ("ERROR: this test crashed when it expected "
                               + "failure")
                    atest.append(so)
                elif atest.attrib["status"] == "pass":
                    atest.append(ET.Element("failure"))
                    so = ET.Element("system-out")
                    so.text = "ERROR: this test passed when it expected failure"
                    atest.append(so)
                elif atest.attrib["status"] == "skip":
                    atest.append(ET.Element("failure"))
                    so = ET.Element("system-out")
                    so.text = ("ERROR: this test skipped when it expected "
                               + "failure")
                    atest.append(so)
                else:
                    raise Exception("test has unknown status: "
                                    + atest.attrib["name"]
                                    + " " + atest.attrib["status"])
            elif expected == "expected-crashes":
                # change error to pass
                if atest.attrib["status"] == "crash":
                    for ftag in atest.findall("error"):
                        atest.remove(ftag)
                    so = ET.Element("system-out")
                    so.text = "Passing test as an expected crash"
                    atest.append(so)
                elif atest.attrib["status"] == "fail":
                    atest.append(ET.Element("failure"))
                    so = ET.Element("system-out")
                    so.text = "ERROR: this test failed when it expected crash"
                    atest.append(so)
                elif atest.attrib["status"] == "pass":
                    atest.append(ET.Element("failure"))
                    so = ET.Element("system-out")
                    so.text = "ERROR: this test passed when it expected crash"
                    atest.append(so)
                elif atest.attrib["status"] == "skip":
                    atest.append(ET.Element("failure"))
                    so = ET.Element("system-out")
                    so.text = "ERROR: this test skipped when it expected crash"
                    atest.append(so)
                else:
                    raise Exception("test has unknown status: "
                                    + atest.attrib["name"]
                                    + " " + atest.attrib["status"])

    for atest in t.findall(".//testcase"):
        atest.attrib["name"] = atest.attrib["name"] + "." + o.hardware + o.arch
    t.write(xml)


class CrucibleTester(object):
    def __init__(self, jobs, env=None):
        self.blacklist = None
        self.jobs = jobs
        self.env = {}
        self.pm = ProjectMap()
        self.opts = Options()
        self.br = self.pm.build_root()
        self.out_dir = self.br + "/../test"
        self.flakes = Flakes(self.pm.current_project(), self.opts)

        if env:
            self.env.update(env)
        if self.opts.arch == "m64":
            icd_name = "intel_icd.x86_64.json"
        elif self.opts.arch == "m32":
            icd_name = "intel_icd.i686.json"
        self.env.update({"LD_LIBRARY_PATH": self.br + "/lib",
                         "VK_ICD_FILENAMES": (self.br
                                              + "/share/vulkan/icd.d/"
                                              + icd_name),
                         "ANV_ABORT_ON_DEVICE_LOSS": "true"})
        self.opts.update_env(self.env)

    def build(self):
        pass

    def clean(self):
        pass

    def test(self):
        if self.opts.arch == "m64":
            icd_name = "intel_icd.x86_64.json"
            hasvk_icd_name = "intel_hasvk_icd.x86_64.json"
        elif self.opts.arch == "m32":
            icd_name = "intel_icd.i686.json"
            hasvk_icd_name = "intel_hasvk_icd.i686.json"

        icd_path = f"{self.pm.build_root()}/share/vulkan/icd.d/{icd_name}"
        hasvk_icd_path = f"{self.pm.build_root()}/share/vulkan/icd.d/{hasvk_icd_name}"
        if os.path.exists(hasvk_icd_path):
            icd_path = f"{icd_path}:{hasvk_icd_path}"

        self.env.update({"LD_LIBRARY_PATH": f"{self.br}/lib",
                         "VK_ICD_FILENAMES": icd_path,
                         "ANV_ABORT_ON_DEVICE_LOSS": "true",

                         # https://gitlab.freedesktop.org/Mesa_CI/mesa_jenkins/-/issues/55
                         "INTEL_COMPUTE_CLASS" : "true",
                         "INTEL_COPY_CLASS" : "true"
                        })

        self.opts.update_env(self.env)
        if not path.exists(self.out_dir):
            os.makedirs(self.out_dir)
        out_xml = (self.out_dir + "/piglit-crucible_" + self.opts.hardware
                   + "_" + self.opts.arch + ".xml")
        include_tests = []
        if self.opts.retest_path:
            include_tests = TestLister(self.opts.retest_path + "/test/").RetestIncludes("crucible-test")

        blacklist = []
        for blacklist_file in get_blacklists():
            blacklist += [l.rstrip() for l in open(blacklist_file)]
        blacklist += self.flakes.test_list()

        # remove commented lines from blacklist and prepend !
        blacklist = [('!' + b) for b in blacklist if (not b.startswith('#') and b)]

        parallelism = []

        if "ivb" in self.opts.hardware:
            parallelism = ['-j', '1']
        elif "byt" in self.opts.hardware:
            parallelism = ['-j', '1']
        elif self.jobs:
            parallelism = ['-j', self.jobs]
        options = parallelism + include_tests + blacklist
        run_batch_command([self.br + "/bin/crucible",
                           "run", "--fork", "--log-pids",
                           "--junit-xml=" + out_xml] + options,
                          env=self.env,
                          expected_return_code=None)
        post_process_results(out_xml)

        self.test_flaky()
        check_gpu_hang()
        Export().export_tests()

    def test_flaky(self):
        """test the flaky crucible tests 10 times"""
        if not self.opts.shard.startswith("1:") and self.opts.shard != "0":
            return
        if "_sim" in self.opts.hardware:
            return
        if self.opts.retest_path:
            return
        assert path.exists(self.out_dir)
        out_xml = f"{self.out_dir}/flaky.xml"
        out, _ = run_batch_command([f"{self.br}/bin/crucible", "ls-tests"],
                                   env=self.env,
                                   streamedOutput=False,
                                   expected_return_code=None,
                                   quiet=True)
        all_tests = [f"{t.decode('utf-8')}.q0" for t in out.split()]
        group = ""
        for test in self.flakes.test_list():
            if not test.endswith('*'):
                continue
            group = test[:-2]
            group_tests = []
            for t in all_tests:
                if t.startswith(group):
                    group_tests.append(t)
            self.flakes.expand_test_group(test, group_tests)

        # don't test flaky tests if they are denied on this platform
        blacklist = []
        for blacklist_file in get_blacklists():
            blacklist += [l.rstrip() for l in open(blacklist_file)]
        for denied in blacklist:
            if denied.endswith('*'):
                denied = denied[:-1]
            self.flakes.deny_test(denied)

        if len(self.flakes.test_list()) == 0:
            # no tests for this platform
            return

        for i in range(10):
            print(f"Running flakes iteration {i}")
            run_batch_command([f"{self.br}/bin/crucible",
                               "run", "--fork", "--log-pids",
                               "--junit-xml=" + out_xml] + self.flakes.test_list(),
                              env=self.env,
                              quiet=True,
                              expected_return_code=None)
            results = ET.parse(out_xml)
            for test in results.findall(".//testcase"):
                test_name = test.attrib['name']
                status = test.attrib["status"]
                if status == "lost":
                    status = "fail"
                if status == "crash":
                    status = "fail"
                if status == "skip":
                    status = "pass"
                assert status in ["pass", "fail"]
                output = ""
                self.flakes.add_result(test_name, status, output)
            os.unlink(out_xml)
        self.flakes.generate_xml()
