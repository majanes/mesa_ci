from ast import literal_eval
from base64 import b64encode
from os import path, environ
from urllib.request import urlopen, Request, HTTPError, URLError
import sys
import time

def cancel_builds(result_path):
    """Cancel all component builds with a matching result_path.  This
    job is used to reliably terminate builds when a scheduler is
    cancelled."""
    base64string = b64encode(bytes(f'{environ["JENKINS_USER"]}:{environ["JENKINS_PW"]}', 'ascii'))
    job_url = "http://mesa-ci-jenkins.jf.intel.com/job/public/job/conformance"

    print("Sleeping for 5s to allow enqueued jobs to be assigned")
    time.sleep(5)
    job_page = literal_eval(urlopen(f"{job_url}/api/python").read().decode("utf-8"))
    max_job = job_page["nextBuildNumber"]
    # check last 400 builds
    for buildnum in range(max_job-1, max_job - 401, -1):
        page_bytes = urlopen(f"{job_url}/{buildnum}/api/python").read()
        build_page = literal_eval(page_bytes.decode("utf-8"))
        for an_action in build_page["actions"]:
            if "parameters" in an_action:
                build_params = an_action["parameters"]
                break
        if not build_params:
            continue
        for a_param in build_params:
            if a_param["name"] == "result_path" and a_param["value"] == result_path:
                # cancel this build
                print(f"cancelling {job_url}/{buildnum}")
                request = Request(f"{job_url}/{buildnum}/stop", method="POST")
                request.add_header("Authorization", f"Basic {base64string.decode('utf-8')}")
                try:
                    urlopen(request)
                except (HTTPError, URLError) as err:
                    print(f"failed to cancel: {err}")

if __name__=="__main__":
    cancel_builds(sys.argv[1])
