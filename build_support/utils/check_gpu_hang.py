import os
import re
import socket
import time
from urllib.request import urlopen
from bisect_test import TestLister
from export.export import Export, fail_and_reboot
from project_map import ProjectMap
from .command import run_batch_command

def check_gpu_hang(identify_test=True):
    # some systems have a gpu hang watchdog which reboots
    # machines, and others do not.   This method checks dmesg,
    # produces a failing test if a hang is found, and schedules a
    # reboot if the host is determined to be a jenkins builder
    # (user=jenkins)
    if os.name == "nt":
        return
    try:
        (out, _) = run_batch_command(["dmesg", "--time-format", "iso"],
                                     quiet=True,
                                     streamedOutput=False)
    except:
        return
    hang_text = ""
    try:
        lines = out.decode()
    except UnicodeDecodeError:
        lines = out.decode("ISO-8859-1")

    for a_line in lines.split('\n'):
        if "gpu hang" in a_line.lower():
            hang_text = a_line
            break
        if "*error* ring create req" in a_line.lower():
            hang_text = a_line
            break
        if "unable to purge gpu memory due lock contention" in a_line.lower():
            hang_text = a_line
            break
        if "i915_gem_userptr" in a_line.lower():
            hang_text = a_line
            break
        if "failed to idle engines, declaring wedged" in a_line.lower():
            hang_text = a_line
            break
        if "resetting rcs0 for hang" in a_line.lower():
            hang_text = a_line
            break
        if "--[ cut here ]--" in a_line.lower():
            hang_text = a_line
            break
        if "Call Trace:" in a_line.lower():
            hang_text = a_line
            break
    if not hang_text:
        return False

    print("ERROR: gpu hang found: " + hang_text)
    project_map = ProjectMap()
    hostname = socket.gethostname()

    # obtain the pid from the hang_text
    br = project_map.build_root()
    m = re.search(r"\[([0-9]+)\]", hang_text)
    if m is not None and identify_test:
        pid = m.group(1)
        test_path = os.path.abspath(br + "/../test")
        test = TestLister(test_path, include_passes=True).TestForPid(pid)
        if test is not None:
            hang_text += "\nHanging Test:\n" + test

    test_path = os.path.abspath(br + "/../test/")
    if not os.path.exists(test_path):
        os.makedirs(test_path)
    # copy full dmesg to output
    with open(f"{test_path}/dmesg_{hostname}.txt", "w") as dmesg_f:
        dmesg_f.writelines(lines)

    fail_and_reboot("gpu-hang-" + hostname, hang_text, project_map)
    return True
